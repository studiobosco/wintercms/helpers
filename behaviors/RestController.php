<?php namespace StudioBosco\Helpers\Behaviors;

use Str;
use Model;
use Config;
use Request;
use Exception;
use Backend\Classes\ControllerBehavior;
use Illuminate\Database\Eloquent\Collection;
use Winter\Storm\Database\ModelException;

/**
 * Rest Controller Behavior
 *
 * Adds REST features for working with backend models.
 *
 * Usage:
 *
 * In the model class definition:
 *
 *   public $implement = ['Mohsin.Rest.Behaviors.RestController'];
 *
 * @author Saifur Rahman Mohsin
 */
class RestController extends ControllerBehavior
{
    use \Backend\Traits\FormModelSaver;

    /**
     * @var Model The child controller that implements the behavior.
     */
    protected $controller;

    /**
     * @var Model The initialized model used by the rest controller.
     */
    protected $model;

    /**
     * @var String The prefix for verb methods.
     */
    protected $prefix = '';

    /**
     * @var array Configuration values that must exist when applying the primary config file.
     * - modelClass: Class name for the model
     * - list: List column definitions
     */
    protected $requiredConfig = ['modelClass', 'allowedActions'];

    /**
     * @var mixed Configuration for this behaviour
     */
    public $restConfig = 'config_rest.yaml';

    /**
     * Behavior constructor
     * @param Backend\Classes\Controller $controller
     */
    public function __construct($controller)
    {
        parent::__construct($controller);

        /*
         * Build configuration
         */
        $this->configPath = $this->guessConfigPathFrom($controller::class);
        $this->config = $this->makeConfig($controller->restConfig ?? $this->restConfig, $this->requiredConfig);
        $this->config->modelClass = Str::normalizeClassName($this->config->modelClass);

        if (isset($this->config->prefix)) {
            $this->prefix = $this->config->prefix;
        }
    }

    public function extendListQuery($query)
    {
        return $query;
    }

    public function extendFindQuery($query)
    {
        return $query;
    }

    public function extendInputBeforeFill(array $data = [])
    {
        return $data;
    }

    public function beforeModelCreate($model)
    {
        return $model;
    }

    public function beforeModelSave($model)
    {
        return $model;
    }

    public function beforeModelDelete($model)
    {
        return $model;
    }

    public function afterModelLoad($model)
    {
        return $model;
    }

    public function beforeModelSend($model)
    {
        return $model;
    }

    /**
     * Creates a new instance of the model. This logic can be changed
     * by overriding it in the rest controller.
     * @return Model
     */
    public function createModelObject()
    {
        return $this->createModel();
    }

    /**
     * Display the records.
     *
     * @return Response
     */
    public function index()
    {
        if (!$this->controller->hasAccessTo('index')) {
            return response()->json('Unauthorized', 401);
        }

        $options = $this->config->allowedActions['index'];
        $scope = array_get($this->config->allowedActions, 'index.scope', null);
        $relations = $this->config->withRelations ?? [];
        $visible = $this->config->visibleAttributes ?? [];
        $hidden = $this->config->hiddenAttributes ?? [];
        $append = $this->config->appendAttributes ?? [];
        $page = intval(Request::input('page', 1));
        $input = Request::input();
        unset($input['page']);
        unset($input['per_page']);

        /*
         * Default options
         */
        extract(array_merge([
            'page'       => $page,
            'pageSize'    => 5,
            'maxPageSize' => 100,
        ], $options));

        $pageSize = min(intval(Request::input('per_page', $pageSize)), $maxPageSize);

        try {
            $model = $this->controller->createModelObject();

            if ($scope) {
                $model = $model->$scope(Request::input());
            }

            $model = $this->controller->extendListQuery($model) ?: $model;

            $result = $this->controller->getPaginator($model->with($relations), $pageSize, $page);

            foreach($result->items() as $record) {
                $record = $this->controller->afterModelLoad($record) ?: $record;
                if (count($visible)) {
                    $this->showAttributes($record, $visible);
                } elseif (count($hidden)) {
                    $this->hideAttributes($record, $hidden);
                }
                $this->appendAttributes($record, $append);

                $record = $this->controller->beforeModelSend($record) ?: $record;
            }

            return response()->json($result, 200);
        }
        catch (Exception $ex) {
            if (Config::get('app.debug')) {
                dd($ex);
            }
            return response()->json($ex->getMessage(), 400);
        }
    }

    /**
     * Store a newly created record using post data.
     *
     * @return Response
     */
    public function store()
    {
        if (!$this->controller->hasAccessTo('store')) {
            return response()->json('Unauthorized', 401);
        }

        $data = Request::all();
        $relations = $this->config->withRelations ?? [];
        $visible = $this->config->visibleAttributes ?? [];
        $hidden = $this->config->hiddenAttributes ?? [];
        $append = $this->config->appendAttributes ?? [];

        try {
            $model = $this->controller->createModelObject();
            $data = $this->controller->extendInputBeforeFill($data);
            $model->fill($data);
            $model = $this->controller->beforeModelCreate($model) ?: $model;
            $model = $this->controller->beforeModelSave($model) ?: $model;
            $model->save();

            // Get relations too
            foreach ($relations as $relation) {
                $model->{$relation};
            }

            if (count($visible)) {
                $this->showAttributes($model, $visible);
            } elseif (count($hidden)) {
                $this->hideAttributes($model, $hidden);
            }

            $this->appendAttributes($model, $append);

            $model = $this->controller->beforeModelSend($model) ?: $model;

            return response()->json($model, 200);
        }
        catch (ModelException $ex) {
            if (Config::get('app.debug')) {
                dd($ex);
            }
            return response()->json($ex->getMessage(), 400);
        }
        catch (Exception $ex) {
            if (Config::get('app.debug')) {
                dd($ex);
            }
            return response()->json($ex -> getMessage(), 400);
        }
    }

    /**
     * Display the specified record.
     *
     * @param  int  $recordId
     * @return Response
     */
    public function show($recordId)
    {
        if (!$this->controller->hasAccessTo('show', $recordId)) {
            return response()->json('Unauthorized', 401);
        }

        $relations = $this->config->withRelations ?? [];
        $visible = $this->config->visibleAttributes ?? [];
        $hidden = $this->config->hiddenAttributes ?? [];
        $append = $this->config->appendAttributes ?? [];

        try {
            $model = $this->controller->findModelObject($recordId);
            $model = $this->controller->afterModelLoad($model) ?: $model;

            // Get relations too
            foreach ($relations as $relation) {
                $model->{$relation};
            }

            if (count($visible)) {
                $this->showAttributes($model, $visible);
            } elseif (count($hidden)) {
                $this->hideAttributes($model, $hidden);
            }

            $this->appendAttributes($model, $append);

            $model = $this->controller->beforeModelSend($model) ?: $model;

            return response()->json($model, 200);
        }
        catch (ModelException $ex) {
            if (Config::get('app.debug')) {
                dd($ex);
            }
            return response()->json($ex->getMessage(), 400);
        }
        catch (Exception $ex) {
            if (Config::get('app.debug')) {
                dd($ex);
            }
            return response()->json($ex->getMessage(), 400);
        }
    }

    /**
     * Update the specified record in using post data.
     *
     * @param  int  $recordId
     * @return Response
     */
    public function update($recordId)
    {
        if (!$this->controller->hasAccessTo('update', $recordId)) {
            return response()->json('Unauthorized', 401);
        }

        $data = Request::all();
        $relations = $this->config->withRelations ?? [];
        $visible = $this->config->visibleAttributes ?? [];
        $hidden = $this->config->hiddenAttributes ?? [];
        $append = $this->config->appendAttributes ?? [];

        try {
            $model = $this->controller->findModelObject($recordId);
            $model = $this->controller->afterModelLoad($model) ?: $model;
            $data = $this->controller->extendInputBeforeFill($data);
            $model->fill($data);
            $model = $this->controller->beforeModelSave($model) ?: $model;
            $model->save();

            // Get relations too
            foreach ($relations as $relation) {
                $model->{$relation};
            }

            if (count($visible)) {
                $this->showAttributes($model, $visible);
            } elseif (count($hidden)) {
                $this->hideAttributes($model, $hidden);
            }

            $this->appendAttributes($model, $append);

            $model = $this->controller->beforeModelSend($model) ?: $model;

            return response()->json($model, 200);
        }
        catch (ModelException $ex) {
            if (Config::get('app.debug')) {
                dd($ex);
            }
            return response()->json($ex->getMessage(), 400);
        }
        catch (Exception $ex) {
            if (Config::get('app.debug')) {
                dd($ex);
            }
            return response()->json($ex->getMessage(), 400);
        }
    }

    /**
     * Patches the specified record in using post data.
     *
     * @param  int  $recordId
     * @return Response
     */
    public function patch($recordId)
    {
        if (!$this->controller->hasAccessTo('patch', $recordId)) {
            return response()->json('Unauthorized', 401);
        }

        return $this->update($recordId);
    }

    /**
     * Remove the specified record.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($recordId)
    {
        if (!$this->controller->hasAccessTo('destroy', $recordId)) {
            return response()->json('Unauthorized', 401);
        }

        try {
            $model = $this->controller->findModelObject($recordId);
            $model = $this->controller->beforeModelDelete($model) ?: $model;
            $model->delete();

            $model = $this->controller->beforeModelSend($model) ?: $model;

            return response()->json($model, 200);
        }
        catch (ModelException $ex) {
            if (Config::get('app.debug')) {
                dd($ex);
            }
            return response()->json($ex->getMessage(), 400);
        }
        catch (Exception $ex) {
            if (Config::get('app.debug')) {
                dd($ex);
            }
            return response()->json($ex->getMessage(), 400);
        }
    }

    /**
     * Finds a Model record by its primary identifier, used by show, update actions.
     * This logic can be changed by overriding it in the rest controller.
     * @param string $recordId
     * @return Model
     */
    public function findModelObject($recordId)
    {
        if (!strlen($recordId)) {
            throw new Exception('Record ID has not been specified.');
        }

        $model = $this->controller->createModelObject();

        /*
         * Prepare query and find model record
         */
        $query = $model->newQuery();
        $this->controller->extendFindQuery($query);
        $result = $query->find($recordId);

        if (!$result) {
            throw new Exception(sprintf('Record with an ID of %u could not be found.', $recordId));
        }

        return $result;
    }

    /**
     * Internal method, prepare the model object
     * @return Model
     */
    protected function createModel()
    {
        $class = $this->config->modelClass;
        return new $class();
    }

    public function appendAttributes($model, $attributes)
    {
        foreach($attributes as $attribute) {
            $parts = explode('.', $attribute);

            while(count($parts)) {
                $part = trim(array_shift($parts));
                if (!strlen($part)) {
                    break;
                }

                if ($model->hasRelation($part)) {
                    if ($model->$part instanceof Model) {
                        $this->appendAttributes($model->$part, [implode('.', $parts)]);
                        break;
                    } elseif ($model->$part instanceof Collection) {
                        foreach($model->$part as $record) {
                            $this->appendAttributes($record, [implode('.', $parts)]);
                        }
                        break;
                    } else {
                        break;
                    }
                } elseif (key_exists($part, $model->getAttributes()) || $model->hasGetMutator($part)) {
                    $model->append($part);
                    break;
                }
            }
        }
    }

    public function showAttributes($model, $visible)
    {
        foreach(array_keys($model->getAttributes()) as $attr) {
            if (!in_array($attr, $visible)) {
                $model->hideAttribute($attr);
            }
        }
    }

    public function hideAttributes($model, $attributes)
    {
        foreach($attributes as $attribute) {
            $parts = explode('.', $attribute);

            while(count($parts)) {
                $part = trim(array_shift($parts));
                if (!strlen($part)) {
                    break;
                }

                if ($model->hasRelation($part)) {
                    if ($model->$part instanceof Model) {
                        $this->hideAttributes($model->$part, [implode('.', $parts)]);
                        break;
                    } elseif ($model->$part instanceof Collection) {
                        foreach($model->$part as $record) {
                            $this->hideAttributes($record, [implode('.', $parts)]);
                        }
                        break;
                    } else {
                        break;
                    }
                } elseif (key_exists($part, $model->getAttributes()) || $model->hasGetMutator($part)) {
                    $model->makeHidden($part);
                    break;
                }
            }
        }
    }

    public function getPaginator($model, $pageSize, $page)
    {
        return $model->paginate($pageSize, $page);
    }

    public function hasAccessTo($action, ...$args)
    {
        return true;
    }
}
