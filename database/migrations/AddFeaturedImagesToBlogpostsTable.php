<?php namespace StudioBosco\Helpers\Database\Migrations;

use Schema;
use October\Rain\Database\Updates\Migration;
use System\Classes\PluginManager;

class AddFeaturedImagesToBlogpostsTable extends Migration
{

    public function up()
    {
        if (PluginManager::instance()->hasPlugin('Winter.Blog')) {
            Schema::table('winter_blog_posts', function ($table) {
                if (!Schema::hasColumn('winter_blog_posts', 'featured_media_images')) {
                    $table->text('featured_media_images')->nullable();
                }
            });
        }
    }

    public function down()
    {
        if (PluginManager::instance()->hasPlugin('Winter.Blog')) {
            Schema::table('winter_blog_posts', function ($table) {
                if (Schema::hasColumn('winter_blog_posts', 'featured_media_images')) {
                    $table->dropColumn('featured_media_images');
                }
            });
        }
    }

}

