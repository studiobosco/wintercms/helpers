<?php namespace StudioBosco\Helpers\Database\Relations;

use Illuminate\Database\Eloquent\Model;
use Winter\Storm\Database\Relations\HasMany;

class HasManyFromArray extends HasMany
{
    protected function getIds()
    {
        $foreignKey = $this->foreignKey;
        $ids = $this->parent->$foreignKey ?? [];

        if (!is_array($ids)) {
            $ids = [$ids];
        }

        return $ids;
    }

    protected function getIdsFrom($models, $key)
    {
        $ids = [];

        foreach($models as $model) {
            if (is_array($model->$key)) {
                $ids = array_merge($ids, $model->$key);
            }
        }

        return $ids;
    }

    protected function setIds(array $ids)
    {
        $foreignKey = $this->foreignKey;
        $this->parent->$foreignKey = array_values($ids);
        $this->parent->save();
    }

    /**
     * Set the base constraints on the relation query.
     *
     * @return void
     */
    public function addConstraints()
    {
        if (static::$constraints) {
            $ids = $this->getIds();
            $query = $this->getRelationQuery();
            $query->whereIn($this->localKey, $ids);

            $query->whereNotNull($this->localKey);
        }
    }

    /**
     * Set the constraints for an eager load of the relation.
     *
     * @param  array  $models
     * @return void
     */
    public function addEagerConstraints(array $models)
    {
        $whereIn = $this->whereInMethod($this->parent, $this->foreignKey);

        $this->getRelationQuery()->{$whereIn}(
            $this->localKey, $this->getIdsFrom($models, $this->foreignKey)
        );
    }

    public function add(Model $model, $sessionKey = null)
    {
        $ids = $this->getIds();
        array_push($ids, $model->id);
        $this->setIds($ids);
    }

    public function addMany($models, $sessionKey = null)
    {
        $ids = $this->getIds();
        $modelIds = is_array($models) ? array_pluck($models, 'id') : $models->pluck('id');
        $ids = array_merge($ids, $modelIds);
        $this->setIds($ids);
    }

    public function remove(Model $model, $sessionKey = null)
    {
        $ids = $this->getIds();
        $ids = array_filter($ids, function ($id) use ($model) {
            return $id != $model->id;
        });

        $this->setIds($ids);
    }
}
