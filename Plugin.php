<?php namespace StudioBosco\Helpers;

use Event;
use Yaml;
use Input;
use Carbon\Carbon;
use Cms\Classes\Theme;
use Cms\Classes\Layout;
use System\Classes\PluginBase;
use System\Classes\PluginManager;

use StudioBosco\Helpers\Classes\LocaleHelper;
use StudioBosco\Helpers\Models\Settings;

/**
 * PagePreview Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'studiobosco.helpers::lang.plugin.name',
            'description' => 'studiobosco.helpers::lang.plugin.description',
            'author'      => 'Studio Bosco <ondrej@studiobosco.de>',
            'icon'        => 'icon-cubes',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->extendStaticPageForms();
        $this->extendBlogPostForm();
        $this->extendFilterScopes();
        $this->extendSystem();
        $this->registerConsoleCommands();
    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'inline_file' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'inlineFile'],
                'file_hash' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'fileHash'],
                'style_for' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'styleFor'],
                'url_without_host' => ['\StudioBosco\Helpers\Classes\UrlHelper', 'urlWithoutHost'],
                'localized_file_path' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'localizedFilePath'],
                'localized_url' => ['\StudioBosco\Helpers\Classes\UrlHelper', 'localizedUrl'],
                'locale_date' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'localeDate'],
                'base64_encode' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'base64Encode'],
                'base64_encode_file' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'base64EncodeFile'],
                'md5' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'md5'],
                'sha1' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'sha1'],
                'url_decode' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'urlDecode'],
                'youtube_video_id' => ['\StudioBosco\Helpers\Classes\SocialMediaHelper', 'getYouTubeVideoId'],
                'vimeo_video_id' => ['\StudioBosco\Helpers\Classes\SocialMediaHelper', 'getVimeoVideoId'],
                'currency' => ['\StudioBosco\Helpers\Classes\CurrencyHelper', 'format'],
            ],
            'functions' => [
                'inline_file' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'inlineFile'],
                'file_hash' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'fileHash'],
                'style_for' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'styleFor'],
                'child_pages_with_layout' => ['\StudioBosco\Helpers\Classes\StaticPagesHelper', 'getChildPagesWithLayout'],
                'parent_page_with_layout' => ['\StudioBosco\Helpers\Classes\StaticPagesHelper', 'getParentPageWithLayout'],
                'page_by_name' => ['\StudioBosco\Helpers\Classes\StaticPagesHelper', 'getPageByName'],
                'inject' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'inject'],
                'render_component' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'renderComponent'],
                'array_set' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'arraySet'],
                'array_get' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'arrayGet'],
                'set_deep' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'setDeep'],
                'get_deep' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'getDeep'],
                'current_url' => ['\StudioBosco\Helpers\Classes\UrlHelper', 'currentUrl'],
                'url_without_host' => ['\StudioBosco\Helpers\Classes\UrlHelper', 'urlWithoutHost'],
                'locale' => ['\StudioBosco\Helpers\Classes\LocaleHelper', 'getLocale'],
                'localized_url' => ['\StudioBosco\Helpers\Classes\UrlHelper', 'localizedUrl'],
                'query_param' => ['\StudioBosco\Helpers\Classes\UrlHelper', 'queryParam'],
                'youtube_video_id' => ['\StudioBosco\Helpers\Classes\SocialMediaHelper', 'getYoutubeVideoId'],
                'vimeo_video_id' => ['\StudioBosco\Helpers\Classes\SocialMediaHelper', 'getVimeoVideoId'],
                'carbon_date' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'carbonDate'],
                'config' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'config'],
                'is_logged_in_backend' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'isLoggedInBackend'],
                'image_size' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'imageSize'],
                'currency' => ['\StudioBosco\Helpers\Classes\CurrencyHelper', 'format'],
                'count_lines' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'countLines'],
                'split_into_lines' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'splitIntoLines'],
                'split_at_line' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'splitAtLine'],
                'split_by_max_lines' => ['\StudioBosco\Helpers\Classes\TwigHelper', 'splitByMaxLines'],
            ],
        ];
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'StudioBosco\Helpers\Components\PageForm' => 'pageForm',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'system.manage_system_settings' => [
                'label' => 'system::lang.permissions.manage_system_settings',
                'tab' => 'system::lang.permissions.name',
                'roles' => [\Backend\Models\UserRole::CODE_DEVELOPER],
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [];
    }

    public function registerFormWidgets()
    {
        return [
            'StudioBosco\Helpers\FormWidgets\Revisions' => 'revisions',
            'StudioBosco\Helpers\FormWidgets\FontAwesomeIcon' => 'fontawesomeicon',
            'StudioBosco\Helpers\FormWidgets\GeoLocation' => 'geolocation',
            'StudioBosco\Helpers\FormWidgets\PartialWithVars' => 'partialwithvars',
            'StudioBosco\Helpers\FormWidgets\Autocomplete' => 'autocomplete',
            'StudioBosco\Helpers\FormWidgets\Clipboard' => 'clipboard',
            'StudioBosco\Helpers\FormWidgets\Uuid' => 'uuid',
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => trans('studiobosco.helpers::lang.settings.label'),
                'description' => trans('studiobosco.helpers::lang.settings.description'),
                'icon'        => 'icon-rocket',
                'class'       => '\StudioBosco\Helpers\Models\Settings',
                'order'       => 100,
                'permissions' => ['system.manage_system_settings'],
            ],
        ];
    }

    protected function extendStaticPageForms()
    {
        // extend static pages with form fields
        Event::listen(
            'backend.form.extendFields',
            function ($widget) {
                if ($widget->model instanceof \Winter\Pages\Classes\Page) {

                    if ($widget->isNested) {
                        return;
                    }

                    $theme = Theme::getActiveTheme();

                    $layoutCode = $widget->model->viewBag['layout'] ?? null;
                    $layout = Layout::load($theme, $layoutCode . '.htm');


                    if (!$layout || !isset($layout->settings['components'])) {
                        return;
                    }

                    $pageFormComponentSettings = null;

                    foreach($layout->settings['components'] as $key => $componentConfig) {
                        if (strpos($key, 'pageForm') === 0) {
                            $pageFormComponentSettings = $componentConfig;
                            break;
                        }
                    }

                    if (!$pageFormComponentSettings) {
                        return;
                    }

                    $formYamlPath = $pageFormComponentSettings['form'] ?? ($layoutCode . '.yaml');
                    $formFilePath = themes_path($theme->getDirName() . '/layouts/' . $formYamlPath);

                    if (!file_exists($formFilePath)) {
                        return;
                    }

                    $formConfig = Yaml::parseFile($formFilePath);

                    if (isset($formConfig['fields'])) {
                        foreach($formConfig['fields'] as $name => $config) {
                            $key = strpos($name, 'viewBag[') === false ? 'viewBag[' . $name . ']' : $name;
                            $widget->addFields([
                                $key => $config,
                            ]);
                        }
                    }

                    if (isset($formConfig['tabs']['fields'])) {
                        foreach($formConfig['tabs']['fields'] as $name => $config) {
                            $key = strpos($name, 'viewBag[') === false ? 'viewBag[' . $name . ']' : $name;
                            $widget->addTabFields([
                                $key => $config,
                            ]);
                        }
                    }

                    if (isset($formConfig['secondaryTabs']['fields'])) {
                        foreach($formConfig['secondaryTabs']['fields'] as $name => $config) {
                            $key = strpos($name, 'viewBag[') === false ? 'viewBag[' . $name . ']' : $name;
                            $widget->addSecondaryTabFields([
                                $key => $config,
                            ]);
                        }
                    }
                }
            }
        );
    }

    protected function extendBlogPostForm()
    {
        // extend static pages with form fields
        Event::listen(
            'backend.form.extendFields',
            function ($widget) {
                if ($widget->model instanceof \Winter\Blog\Models\Post) {

                    if ($widget->isNested) {
                        return;
                    }

                    if ($widget->getField('content') && isset($widget->secondaryTabs['fields']['content'])) {
                        $widget->addSecondaryTabFields([
                            'content' => array_merge([], $widget->secondaryTabs['fields']['content'], [
                                'type' => LocaleHelper::hasMulitpleLocales() ? 'mlricheditor' : 'richeditor',
                            ]),
                        ]);
                    }

                    if (Settings::get('use_media_images_in_blog_posts', false) && $widget->getField('featured_images')) {
                        $widget->addTabFields([
                            'featured_media_images' => [
                                'label' => $widget->tabs['fields']['featured_images']['label'],
                                'tab' => $widget->tabs['fields']['featured_images']['tab'],
                                'type' => 'repeater',
                                'mode' => 'grid',
                                'form' => [
                                    'fields' => [
                                        'image' => [
                                            'label' => trans('backend::lang.editor.image'),
                                            'type' => 'mediafinder',
                                            'mode' => 'image',
                                            'imageWidth' => 200,
                                            'imageHeight' => 200,
                                        ],
                                        'alt' => [
                                            'label' => trans('backend::lang.fileupload.description_label'),
                                        ],
                                    ],
                                ],
                            ],
                        ]);

                        $widget->removeField('featured_images');
                    }
                }
            }
        );

        if (PluginManager::instance()->hasPlugin('Winter.Blog')) {
            \Winter\Blog\Models\Post::extend(function ($model) {
                if (Settings::get('use_media_images_in_blog_posts', false)) {
                    $model->addJsonable('featured_media_images');
                }
            });
            \Winter\Blog\Controllers\Posts::extend(function ($controller) {
                $controller->addDynamicMethod('formBeforeUpdate', function ($model) use ($controller) {
                    if (Settings::get('use_media_images_in_blog_posts', false)) {
                        if (!Input::has('Post.featured_media_images')) {
                            $model->featured_media_images = null;
                        }
                    }
                });
            });
        }
    }

    protected function extendFilterScopes()
    {
        Event::listen(
            'backend.filter.extendScopes',
            function (\Backend\Widgets\Filter $filterWidget) {
                foreach($filterWidget->config->scopes as $key => $config) {
                    if (isset($config['type']) && $config['type'] === 'daterange' && isset($config['default']) && is_array($config['default'])) {
                        $config['default'] = array_map(function ($value) {
                            return Carbon::parse($value);
                        }, $config['default']);

                        $filterWidget->addScopes([
                            $key => $config,
                        ]);
                    } elseif (isset($config['type']) && $config['type'] === 'date' && isset($config['default'])) {
                        $config['default'] = Carbon::parse($config['default']);
                        $filterWidget->addScopes([
                            $key => $config,
                        ]);
                    }
                }
            }
        );
    }

    protected function extendSystem()
    {
        \System\Controllers\Settings::extend(function ($controller) {
            $controller->requiredPermissions[] = 'system.manage_system_settings';
        });

        Event::listen('backend.menu.extendItems', function (\Backend\Classes\NavigationManager $navigationManager) {
            $items = $navigationManager->listMainMenuItems();
            $systemItem = $items['WINTER.SYSTEM.SYSTEM'];
            $systemItem->permissions[] = 'system.manage_system_settings';
            $navigationManager->removeMainMenuItem('Winter.System', 'system');
            $navigationManager->addMainMenuItem('Winter.System', 'system', [
                'label' => $systemItem->label,
                'icon' => $systemItem->icon,
                'iconSvg' => $systemItem->iconSvg,
                'counter' => $systemItem->counter,
                'counterLabel' => $systemItem->counterLabel,
                'badge' => $systemItem->badge,
                'url' => $systemItem->url,
                'permissions' => $systemItem->permissions,
                'order' => $systemItem->order,
                'sideMenu' => $systemItem->sideMenu,
            ]);
        });
    }

    protected function registerConsoleCommands()
    {
        $this->registerConsoleCommand('helpers:export:translations', \StudioBosco\Helpers\Console\ExportTranslations::class);
    }
}
