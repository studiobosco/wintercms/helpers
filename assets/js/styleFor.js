import loadFile from './loadFile.js';

let styles = {};

/**
* Loads styles from one or more styles.json files
*
* @param {String} ...stylesPaths
*/
async function loadStyles(...stylesPaths) {
  let stylesPath;

  while(stylesPath = stylesPaths.shift()) {
      const _styles = await loadFile(
          stylesPath,
          { as: 'json' }
      );
      styles = _.merge(styles, _styles);
  }
}

/**
* Returns the style for a given key
*
* @param {*} key
* @param {*} separator
* @returns
*/
function styleFor(key, separator = ' ') {
    let value = _.get(styles, key, '').replace(/\s/g, separator)

    // resolve style references
    const matches = value.matchAll(/\$\{([a-zA-Z0-9\.\_\-]+)\}/gm);

    for (const match of matches) {
        value = value.replace(match[0], styleFor(match[1], separator));
    }

    return separator === '.' ? '.' + value : value;
}

export const loadStyles = loadStyles;
export const styleFor = styleFor;
