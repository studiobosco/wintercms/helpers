/**
 * Returns the path to an ES6 module.
 * 
 * @param {String} url 
 * @returns 
 */
function getModulePath(url) {
    let parts = url.split('/');
    parts.pop();
    return parts.join('/');
}

export default getModulePath;