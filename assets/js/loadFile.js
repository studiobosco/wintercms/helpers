/**
 * Loads a file async
 * @param {String} url
 * @param {Object} opts Options {as: 'text'|'json'}
 * @returns Promise with the file contents either as text or json.
 */
async function loadFile(url, opts = {}) {
    const as = opts.as || 'text';
    const reqOpts = {};

    if (opts.as === 'json') {
        reqOpts.headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        };
    }

    return fetch(url, reqOpts)
    .then((res) => {
        if (res.ok) {
            switch(as) {
                case 'text':
                    return res.text();
                case 'json':
                    return res.json();
                default:
                    return res.text();
            }
        } else {
            return res.text()
            .then((msg) => {
                throw new Error(msg);
            });
        }
    });
}

export default loadFile;
