/**
 * Requests an URL async
 * @param {String} url
 * @param {Object} opts Options {as: 'text'|'json'} + all fetch() options
 * @returns Promise with the file contents either as text or json.
 */
async function requestUrl(url, opts = {}) {
    const as = opts.as || 'text';

    if (opts.as === 'json') {
        if (!opts.headers) {
            opts.headers = {};
        }
        opts.headers['Content-Type'] = 'application/json';
        opts.headers['Accept'] = 'application/json';
    }

    return fetch(url, opts)
    .then((res) => {
        if (res.ok) {
            switch(as) {
                case 'text':
                    return res.text();
                case 'json':
                    return res.json();
                default:
                    return res.text();
            }
        } else {
            return res.text()
            .then((msg) => {
                throw new Error(msg);
            });
        }
    });
}

export default requestUrl;
