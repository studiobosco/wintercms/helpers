<?php

namespace StudioBosco\Helpers\Classes;

use Lang;
use Input;
use Request;
use BackendAuth;
use Config;
use Carbon\Carbon;
use Cms\Classes\Theme;

class TwigHelper
{
    public static array $styles;

    public static function styleFor($key)
    {
        if (!isset(self::$styles)) {
            $theme = Theme::getActiveTheme();
            self::$styles = json_decode(
                file_get_contents($theme->getPath() . '/' . Config::get('cms.themeStylesPath', 'assets/styles.json')),
                true
            );
        }

        try {
            $style = array_get(self::$styles, $key, '');

            if (!is_string($style)) {
                return 'Style ' . $key . ' not found.';
            }

            $matches = [];
            preg_match_all('/\$\{([a-zA-Z0-9\.\_\-]+)\}/', $style, $matches, PREG_SET_ORDER);

            foreach($matches as $match) {
                $style = str_replace_first($match[0], self::styleFor($match[1]), $style);
            }

            return $style;
        } catch (\Exception $err) {
            return $err->toString();
        }
    }

    public static function inlineFile($path)
    {
        $fullPath = base_path(ltrim($path, '/'));
        return file_get_contents($fullPath);
    }

    public static function fileHash($path, $algo = 'md5')
    {
        $fullPath = base_path(ltrim($path, '/'));
        if (file_exists($fullPath)) {
            return hash_file($algo, $fullPath);
        } else {
            return '';
        }
    }

    public static function arraySet(array $arr, string $path, $value)
    {
        array_set($arr, $path, $value);
        return $arr;
    }

    public static function arrayGet(array $arr, string $path, $defaultValue)
    {
        return array_get($arr, $path, $defaultValue);
    }

    public static function getDeep($value, string $path, $defaultValue = null)
    {
        $parts = explode('.', $path);

        foreach($parts as $part) {
            if (is_array($value)) {
                $value = $value[$part] ?? null;
            } elseif ($value instanceof \Illuminate\Database\Eloquent\Collection) {
                $value = $value[$part] ?? null;
            } elseif (is_object($value)) {
                $value = $value->$part ?? null;
            } else {
                return $defaultValue;
            }
        }

        return $value;
    }

    public static function setDeep(&$target, string $path, $value)
    {
        $parts = explode('.', $path);
        $_target = $target;
        $lastPart = array_pop($parts);

        foreach($parts as $part) {
            if (is_array($_target)) {
                $_target = &$_target[$part];
            } elseif ($_target instanceof \Illuminate\Database\Eloquent\Collection) {
                $_target = $_target[$part];
            } elseif (is_object($_target)) {
                $_target = $_target->$part;
            } else {
                return;
            }
        }

        if (is_array($_target)) {
            $_target[$lastPart] = $value;
        } elseif ($_target instanceof \Illuminate\Database\Eloquent\Collection) {
            $_target[$part] = $value;
        } elseif (is_object($_target)) {
            $_target->$part = $value;
        }

        return $target;
    }

    public static function inject($className, ...$args)
    {
        return new $className(...$args);
    }

    public static function renderComponent($name, $page, $properties = [])
    {
        if (!$page) {
            return null;
        }

        $origPageComponents = null;
        $controller = $page->controller ?? $page->getController();
        $page = $controller->getPage();
        $pageObj = $controller->getPageObject();
        $layout = $pageObj->getLayout();
        $alias = $properties['alias'] ?? $name;
        unset($properties['alias']);
        $componentObj = $page->components[$alias] ?? $layout->components[$alias] ?? null;

        if (!$componentObj) {
            $componentObj = \Cms\Classes\ComponentManager::instance()->makeComponent(
                $name,
                $pageObj,
                $properties
            );

            $componentObj->id = uniqid($name);
            $componentObj->alias = $alias;

            $origPageComponents = $page->components;
            $page->components = array_merge($page->components, [
                $name => $componentObj,
            ]);
        } else {
            foreach($properties as $attr => $value) {
                $componentObj->setProperty($attr, $value);
            }
        }

        if (method_exists($componentObj, 'init')) {
            $componentObj->init();
        }
        $componentObj->onRun();
        $result = $controller->renderComponent($name);

        // reset page componentents
        if ($origPageComponents) {
            $page->components = $origPageComponents;
        }

        return $result;
    }

    public static function localeDate($date, $format, $locale = null)
    {
        $date = new \DateTime($date);
        $locale = $locale ?? Lang::getLocale();
        $formatter = new \IntlDateFormatter(
            $locale,
            \IntlDateFormatter::NONE,
            \IntlDateFormatter::NONE
        );
        $formatter->setPattern($format);
        return $formatter->format($date);
    }

    public static function localizedFilePath($filepath, $locale = null)
    {
        $ext = pathinfo($filepath, PATHINFO_EXTENSION);
        $defaultLocale = \Winter\Translate\Classes\Translator::instance()->getDefaultLocale();

        if (!$locale || $locale === $defaultLocale) {
        return $filepath;
        }

        return substr($filepath, 0, strlen($filepath) -(strlen('.' . $ext))) . '_' . $locale . '.' . $ext;
    }

    public static function carbonDate($date = null)
    {
        return new Carbon($date);
    }

    public static function base64Encode($str)
    {
        return base64_encode($str);
    }

    public static function base64EncodeFile($path, $pathIsUrl = false)
    {
        if ($pathIsUrl) {
            $fullPath = url($path);
        } else {
            $fullPath = base_path($path);
        }

        return base64_encode(file_get_contents($fullPath));
    }

    public static function md5($value)
    {
        return md5($value);
    }

    public static function sha1($value)
    {
        return sha1($value);
    }

    public static function urlDecode($value)
    {
        return urldecode($value);
    }

    public static function config($key, $default = null)
    {
        return config($key, $default);
    }

    public static function isLoggedInBackend()
    {
        return BackendAuth::getUser() ? true : false;
    }

    public static function imageSize($path, $options = [])
    {
        $absolute = array_get($options, 'absolute', false);
        if (!$absolute) {
            $path = base_path(ltrim($path, '/'));
        }

        if (!file_exists($path) || !is_file($path)) {
            return [
                'width' => 0,
                'height' => 0,
                'aspect_ratio' => 0,
            ];
        }

        $info = getimagesize($path);

        if (is_array($info)) {
            return [
                'width' => $info[0],
                'height' => $info[1],
                'aspect_ratio' => $info[0] / $info[1],
            ];
        } else {
            return [
                'width' => 0,
                'height' => 0,
                'aspect_ratio' => 0,
            ];
        }
    }

    public static function countLines(
        $html,
        $maxCharsPerLine = 80,
        $newLineTokens = ['<br>', '<br/>', '</p>', '</ul>' , '</ol>', '</li>', '</h1>', '</h2>', '</h3>', '</h4>', '</h5>', '</h6>', '</div>', '</blockquote>']
    ) {
        $countLines = 0;
        $htmlLower = strtolower($html);

        foreach($newLineTokens as $newLineToken) {
            $countLines += substr_count($htmlLower, $newLineToken);
        }

        $countLines += ceil(strlen(strip_tags($html)) / $maxCharsPerLine);

        return $countLines;
    }

    protected static function getUnclosedTags($html)
    {
        $selfClosingTags = ['img', 'br', 'hr', 'input', 'link', 'area', 'base', 'col', 'embed', 'meta', 'param', 'source', 'track', 'wbr'];

        // collect opened tags in the line before split
        $lowerHtml = strtolower($html);
        $matches = [];
        $openedTags = [];
        preg_match_all('#<([a-z]+)[^>]*>#m', $lowerHtml, $matches, PREG_SET_ORDER);

        if (count($matches)) {
            foreach($matches as $match) {
                if (!in_array($match[1], $selfClosingTags)) {
                    $openedTags[] = $match;
                }
            }
        }

        $openedTagsCount = [];

        foreach($openedTags as $match) {
            if (!isset($openedTagsCount[$match[1]])) {
                $openedTagsCount[$match[1]] = 0;
            }
            $openedTagsCount[$match[1]]++;
        }

        foreach($openedTagsCount as $openedTag => $openedCount) {
            $closedMatches = [];
            preg_match_all('#<(\/' . $openedTag . ')>#', $lowerHtml, $closedMatches, PREG_SET_ORDER);
            $closedCount = count($closedMatches);
            $countDiff = $openedCount - $closedCount;

            // remove tags that got closed from the opened tags
            if ($closedCount > 0) {
                foreach($openedTags as $index => $match) {
                    if ($openedTag === $match[1] && $openedCount > $countDiff) {
                        unset($openedTags[$index]);
                        $openedCount--;
                    }
                }
            }
        }

        $openedTags = array_values($openedTags);

        return $openedTags;
    }

    public static function splitIntoLines(
        $html,
        $maxCharsPerLine = 80,
        $newLineTokens = ['<br>', '<br/>', '</p>', '</ul>' , '</ol>', '</li>', '</h1>', '</h2>', '</h3>', '</h4>', '</h5>', '</h6>', '</div>', '</blockquote>']
    ) {
        $lines = [];
        $index = 0;
        $lineCharCount = 0;
        $tagStarted = false;

        while(strlen($html)) {
            // exit if last line reached
            if (strlen(strip_tags($html)) < $maxCharsPerLine) {
                $lines[] = $html;
                $html = '';
                $index = 0;
                $lineCharCount = 0;
                $char = null;
                break;
            }

            // split line if max char count per line is reached
            if ($lineCharCount > $maxCharsPerLine) {
                $lines[] = substr($html, 0, $index);

                // cut off the line from the html code
                $html = substr($html, $index);

                // reset the counters
                $index = 0;
                $lineCharCount = 0;
                $char = null;
            }
            // otherwhise count the chars in the current line
            else {
                // detect tokens that create a new line
                foreach($newLineTokens as $newLineToken) {
                    if (substr(strtolower($html), $index, strlen($newLineToken)) === $newLineToken) {
                        $lines[] = substr($html, 0, $index + strlen($newLineToken));
                        $html = substr($html, $index + strlen($newLineToken));
                        $index = 0;
                        $lineCharCount = 0;
                        $char = null;
                        $tagStarted = false;
                        continue;
                    }
                }

                // detect tags
                $char = substr($html, $index, 1);

                if ($tagStarted && $char === '>') {
                    $tagStarted = false;
                }
                elseif (!$tagStarted && $char === '<') {
                    $tagStarted = true;
                }
                elseif (!$tagStarted) {
                    // only count characters that do not belong to tags
                    $lineCharCount++;
                }

                // continue to next char in the line
                $index++;
            }
        }

        return $lines;
    }

    public static function splitAtLine(
        $html,
        $atLine,
        $maxCharsPerLine = 80,
        $newLineTokens = ['<br>', '<br/>', '</p>', '</ul>' , '</ol>', '</li>', '</h1>', '</h2>', '</h3>', '</h4>', '</h5>', '</h6>', '</div>', '</blockquote>']
    ) {
        $lines = self::splitIntoLines($html, $maxCharsPerLine, $newLineTokens);

        $countLines = count($lines);
        $parts = [];
        $openedTags = [];

        // number of lines is within max lines
        // so return a single part with all lines
        if ($countLines <= $atLine) {
            return [$html];
        }

        $parts[] = implode('', array_slice($lines, 0, $atLine));
        $parts[] = implode('', array_slice($lines, $atLine));

        // collect opened tags in the line before split
        $openedTags = self::getUnclosedTags($parts[0]);

        // first all opened tags have to be closed in the line before the split
        foreach(array_reverse($openedTags) as $openedTag) {
            $parts[0] .= '</' . $openedTag[1] . '>';
        }

        // tags have to be reopened again in the line after the split
        if (isset($lines[$atLine])) {
            $tags = '';

            foreach($openedTags as $openedTag) {
                $tags .= $openedTag[0];
            }

            // prepend tags to next line
            $parts[1] = $tags . $parts[1];
        }

        return $parts;
    }

    public static function splitByMaxLines(
        $html,
        $maxLines,
        $maxCharsPerLine = 80,
        $newLineTokens = ['<br>', '<br/>', '</p>', '</ul>' , '</ol>', '</li>', '</h1>', '</h2>', '</h3>', '</h4>', '</h5>', '</h6>', '</div>', '</blockquote>']
    ) {
        $lines = self::splitIntoLines($html, $maxCharsPerLine, $newLineTokens);

        $countLines = count($lines);
        $parts = [];
        $lastSplitIndex = 0;

        // number of lines is within max lines
        // so return a single part with all lines
        if ($countLines <= $maxLines) {
            return [$html];
        }

        $openedTags = [];

        foreach($lines as $index => $line) {
            // reached last split, beacause remaining lines are less or equal to max
            if ($countLines - $lastSplitIndex <= $maxLines) {
                $part = implode('', array_slice($lines, $lastSplitIndex));

                // previously opened tags have to be reopened
                if (count($parts) && count($openedTags)) {
                    $tags = '';
                    foreach($openedTags as $openedTag) {
                        $tags .= $openedTag[0];
                    }

                    $part = $tags . $part;
                }

                $parts[] = $part;
                break;
            }
            // new split has to be created, as the number of lines since the last split is more then max
            elseif ($index - $lastSplitIndex === $maxLines) {
                $part = implode('', array_slice($lines, $lastSplitIndex, $maxLines));

                // previously opened tags have to be reopened
                if (count($parts) && count($openedTags)) {
                    $tags = '';
                    foreach($openedTags as $openedTag) {
                        $tags .= $openedTag[0];
                    }

                    $part = $tags . $part;
                }

                $openedTags = self::getUnclosedTags($part);
                $tags = '';

                // all opened tags have to be closed in the new split
                foreach(array_reverse($openedTags) as $openedTag) {
                    $tags .= '</' . $openedTag[1] . '>';
                }
                $part .= $tags;
                $parts[] = $part;

                // update last split index
                $lastSplitIndex = $index;
            }
        }

        return $parts;
    }
}
