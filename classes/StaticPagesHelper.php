<?php

namespace StudioBosco\Helpers\Classes;

use Cms\Classes\Theme;
use Winter\Pages\Classes\Page;

class StaticPagesHelper
{
    protected static function _getStaticPageOptions($parent = null, $level = 0)
    {
        $theme = Theme::getActiveTheme();
        $pages = $parent ? $parent->getChildren() : array_filter(Page::listInTheme($theme)->all(), function ($page) {
            return !$page->getParent();
        });

        $indent = '';

        for($l = 0; $l < $level; $l++) {
            $indent .= "&nbsp;&nbsp;";
        }

        $options = [];
        foreach ($pages as $page) {
            $key = $page->getBaseFileName();
            $options[$key] = $indent . self::_getNestedStaticPageTitle($page);

            $options = array_merge($options, self::_getStaticPageOptions($page, $level + 1));
        }

        return $options;
    }

    protected static function _getNestedStaticPageTitle($page, $suffix = '')
    {
        $title = $page->viewBag['title'] . ($suffix ? ' / ' . $suffix : '');

        if ($page->getParent()) {
            $title = self::_getNestedStaticPageTitle($page->getParent(), $title);
        }

        return $title;
    }

    public static function getStaticPageOptions()
    {
        return self::_getStaticPageOptions();
    }

    public static function getPagesWithLayout($layout, $visibleOnly = true)
    {
        if (!$layout) {
            return [];
        }

        $theme = Theme::getActiveTheme();
        $pages = array_filter(Page::listInTheme($theme)->all(), function ($page) use ($layout, $visibleOnly) {
            if ($visibleOnly && intval($page->getViewBag()->property('is_hidden'))) {
                return false;
            }
            return $page->getViewBag()->property('layout') === $layout;
        });

        return $pages;
    }

    public static function getChildPagesWithLayout($page, $layout, $visibleOnly = true)
    {
        if (!$page) {
            return [];
        }
        return array_filter($page->getChildren(), function ($page) use ($layout, $visibleOnly) {
            if ($visibleOnly && intval($page->getViewBag()->property('is_hidden'))) {
                return false;
            }
            return $page->getViewBag()->property('layout') === $layout;
        });
    }

    public static function getParentPageWithLayout($page, $layout, $visibleOnly = true)
    {
        if (!$page) {
            return null;
        }

        if (
            $page->getViewBag()->property('layout') === $layout &&
            ($visibleOnly && !intval($page->getViewBag()->property('is_hidden')))
        ) {
            return $page;
        } elseif ($page->getParent()) {
            return self::getParentPageWithLayout($page->getParent(), $layout, $visibleOnly);
        }

        return null;
    }

    protected static $pageNameHash = [];

    public static function clearPageNameHash()
    {
        self::$pageNameHash = [];
    }

    private static function pageNamesToHash($theme)
    {
        $pages = Page::listInTheme($theme);
        $pagesArray = $pages->keyBy(function ($page) {
            return $page->getBaseFileName();
        })->all();
        return $pagesArray;
    }

    public static function getPageByName($name)
    {
        $themeId = Theme::getActiveTheme()->getId();
        if(!isset(self::$pageNameHash[$themeId])) {
            self::$pageNameHash[$themeId] = self::pageNamesToHash($themeId);
        }
        $page = self::$pageNameHash[$themeId][$name] ?? null;
        return $page;
    }

    public static function getStaticMenuOptions()
    {
        $theme = Theme::getActiveTheme();
        $menus = \Winter\Pages\Classes\Menu::listInTheme($theme);
        $options = [];

        foreach($menus as $menu) {
            $options[$menu->code] = $menu->name;
        }

        return $options;
    }
}
