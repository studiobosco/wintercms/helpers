<?php

namespace StudioBosco\Helpers\Classes;

use Input;
use Request;
use Cms\Classes\Theme;

use StudioBosco\Helpers\Classes\LocaleHelper;
use StudioBosco\TranslateExtended\Models\Settings as TranslateExtendedSettings;

class UrlHelper
{
    public static function currentUrl()
    {
        return Request::url();
    }

    public static function urlWithoutHost($url)
    {
        if (strpos($url, '://') === false) {
            return $url;
        }

        $parts = explode('://', $url, 2);
        $url = $parts[array_key_last($parts)];
        $parts = explode('/', $url, 2);
        return '/' . $parts[array_key_last($parts)];
    }

    public static function queryParam($name)
    {
        return Input::get($name);
    }

    public static function localizedUrl($url, $withoutHost = true)
    {
        if ($withoutHost) {
            $url = self::urlWithoutHost($url);
        }

        if (!intval(TranslateExtendedSettings::get('route_prefixing'))) {
            return $url;
        }

        $locale = LocaleHelper::getLocale();

        return '/' . $locale . $url;
    }
}
