<?php

namespace StudioBosco\Helpers\Classes;

use System\Classes\PluginManager;
use Winter\Translate\Classes\Translator;

class LocaleHelper
{
    public static function getLocale()
    {
        $translator = Translator::instance();
        return $translator->getLocale() ?? $translator->getDefaultLocale();
    }

    public static function getDefaultLocale()
    {
        $translator = Translator::instance();
        return $translator->getDefaultLocale();
    }

    public static function hasTranslatePlugin()
    {
        return PluginManager::instance()->hasPlugin('Winter.Translate');
    }

    public static function hasMulitpleLocales()
    {
        return \Winter\Translate\Models\Locale::isAvailable();
    }

    public static function getLocaleOptions()
    {
        return \Winter\Translate\Models\Locale::listAvailable();
    }
}
