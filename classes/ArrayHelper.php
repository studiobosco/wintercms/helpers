<?php namespace StudioBosco\Helpers\Classes;

class ArrayHelper
{
    public static function uniqueBy($array, $by) {
        $values = [];

        return array_filter($array, function ($item) use ($by, &$values) {
            $value = array_get($item, $by);

            if (in_array($value, $values)) {
                return false;
            } else {
                $values[] = $value;
                return true;
            }
        });
    }
}
