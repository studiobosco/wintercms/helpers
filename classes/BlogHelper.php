<?php

namespace StudioBosco\Helpers\Classes;

use System\Classes\PluginManager;

class BlogHelper
{
    protected static function _addChildCategories($category, &$options, $level = 1) 
    {
        $indent = str_repeat("   ", $level);

        foreach($category->children as $childCategory) { 
            $options[$childCategory->id] = $indent . $childCategory->name;
            self::_addChildCategories($childCategory, $options, $level + 1);
        }
    }

    protected static function _getPostCategories($post) 
    {
        if (sizeof($post->categories) > 0) {
            foreach($post->categories as $category) { 
                $categories[] = $category->name;
            }
            return $categories;
        }
        
        return [];
    }

    protected static function _getPostTags($post) 
    {
        if (sizeof($post->tags) > 0) {
            foreach($post->tags as $tag) { 
                $tags[] = $tag->name;
            }
            return $tags;
        } 

        return [];
    }

    public static function getAllBlogCategoryOptions() 
    {
        $categories = \Winter\Blog\Models\Category::getNested();

        $options = [];

        foreach($categories as $category) { 
            $options[$category->id] = $category->name;
            self::_addChildCategories($category, $options);
        }

        return $options;
    }

    public static function getAllBlogTagOptions() 
    {
        $tags = \GinoPane\BlogTaxonomy\Models\Tag::listFrontend([]);

        $options = [];

        foreach($tags as $tag) { 
            $options[$tag->id] = $tag->name;
        }

        return $options;
    }

    public static function getAllBlogPageOptions()
    {
        $posts = [];
        $options = [];
        $categories = [];
        $usesBlogTaxonomyPlugin = PluginManager::instance()->hasPlugin('GinoPane.BlogTaxonomy');
        
        if ($usesBlogTaxonomyPlugin) {
            $posts = \Winter\Blog\Models\Post::with(['featured_images'])->listFrontEnd([ 'perPage' => 1000 ])->sortByDesc('published_at');
            $tags = \GinoPane\BlogTaxonomy\Models\Tag::listFrontend([]);
        } else {
            $posts = \Winter\Blog\Models\Post::with(['categories', 'featured_images'])->listFrontEnd([ 'perPage' => 1000 ])->sortByDesc('published_at');
        }

        foreach($posts as $post) { 
            $options[$post->id] = $post->title . "  |  " .
                $post->published_at->format('d.m.Y') . "  |  " .
                (($usesBlogTaxonomyPlugin && $post->post_type) ? $post->post_type->name . "  |  " : "") .
                (($usesBlogTaxonomyPlugin && sizeof($post->tags)) ? implode(", ",self::_getPostTags($post)) : implode(", ",self::_getPostCategories($post)));
        }
        
        return $options;
    }
}
