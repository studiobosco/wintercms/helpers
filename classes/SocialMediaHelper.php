<?php

namespace StudioBosco\Helpers\Classes;

class SocialMediaHelper
{
    public static function getYouTubeVideoId($url)
    {
        $id = null;

        // regular watch link
        if (strpos($url, '/watch?') !== false) {
            $query = [];
            parse_str(
                parse_url($url, PHP_URL_QUERY), $query
            );
            $id = array_get($query, 'v', null);
        }
        // YouTube short link
        elseif (strpos($url, '//youtu.be/') !== false) {
            $id = array_last(
                explode('/', parse_url($url, PHP_URL_PATH))
            );
        }

        return $id;
    }

    public static function getVimeoVideoId($url)
    {
        $id = null;

        // regular vimeo video link
        if (strpos($url, '//vimeo.com/') !== false) {
            $id = array_last(
                explode('/', parse_url($url, PHP_URL_PATH))
            );
        }

        return $id;
    }
}
