<?php namespace StudioBosco\Helpers\Classes;

class DebugHelper
{
    protected static $timers = [];

    protected static function setTimer($key, float $time)
    {
        self::$timers[$key] = $time;
    }

    protected static function getTimer($key)
    {
        return self::$timers[$key] ?? null;
    }

    protected static function unsetTimer($key)
    {
        unset(self::$timers[$key]);
    }

    public static function startTimer($name)
    {
        self::setTimer($name, microtime(true));
    }

    public static function stopTimer($name)
    {
        $end = microtime(true);
        $start = self::getTimer($name);
        $duration = $end - $start;
        self::setTimer($name, $duration);

        return $duration;
    }

    public static function formatTimerDuration($duration, $decimals = 4)
    {
        return round($duration, $decimals) . 's';
    }

    public static function flushTimers()
    {
        $timers = self::$timers;
        self::$timers = [];
        return $timers;
    }
}
