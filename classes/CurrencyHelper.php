<?php namespace StudioBosco\Helpers\Classes;

use Lang;
use NumberFormatter;

class CurrencyHelper
{
    public static function getCurrencies()
    {
        $currencies = json_decode(file_get_contents(__DIR__ . '/../data/currencies.json'));
        $options = [];

        foreach($currencies as $currency) {
          $options[$currency->cc] = $currency;
        }

        return $options;
    }

    public static function format($amount = 0, string $currencyCode = null, $locale = null, $decimals = 2)
    {
        $amount = floatval($amount) ?? 0;
        $formatter = new NumberFormatter($locale ? $locale : Lang::locale(), NumberFormatter::CURRENCY);
        $formatter->setAttribute(NumberFormatter::MIN_FRACTION_DIGITS, $decimals);
        return $formatter->formatCurrency($amount, $currencyCode);
    }
}
