<?php

namespace StudioBosco\Helpers\Classes;

class FormHelper
{
    protected static $fontAwesomeIcons = null;

    public static function getFontAwesomeIconOptions()
    {
        $options = ['' => '-'];

        if (!self::$fontAwesomeIcons) {
            self::$fontAwesomeIcons = json_decode(file_get_contents(__DIR__ . '/../assets/FontAwesome-v5.0.9-Free.json'), true);
        }

        foreach(self::$fontAwesomeIcons as $iconClass) {
            $options[$iconClass] = $iconClass;
        }

        return $options;
    }
}
