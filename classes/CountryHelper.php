<?php namespace StudioBosco\Helpers\Classes;

use Lang;
use Backend\Models\Preference;

class CountryHelper
{
    public static function getCountries($locale = null)
    {
        $locale = $locale ?? Lang::locale();

        return require(__DIR__ . '/../lang/' . $locale . '/country.php');
    }

    public static function locales()
    {
        $preference = new Preference;
        return $preference->getLocaleOptions();
    }
}
