<?php namespace StudioBosco\Helpers\FormWidgets;

use Backend\Classes\FormWidgetBase;

/**
 * PartialWithVars Form Widget
 */
class PartialWithVars extends FormWidgetBase
{
    public $path = null;
    public $vars = [];

    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'partialwithvars';

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->fillFromConfig([
            'vars',
            'path',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('partialwithvars');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['formField'] = $this->formField;
        $this->vars['formModel'] = $this->model;
        $this->vars['path'] = $this->path;
        $this->vars['vars'] = $this->vars;
    }
}
