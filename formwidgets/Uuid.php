<?php namespace StudioBosco\Helpers\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Ramsey\Uuid\Uuid as _Uuid;

class Uuid extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'uuid';

    public $hidden = true;

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->fillFromConfig([
            'hidden',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('uuid');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $value = $this->getLoadValue();
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $value ? $value : _Uuid::uuid4()->toString();
    }
}
