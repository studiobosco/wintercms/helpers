function filterFontAwesomeIcons(input, targetId) {
    const target = document.getElementById(targetId);
    const q = input.value.trim().toLowerCase();

    if (q.length === 0) {
        return;
    }

    const icons = target.querySelectorAll('*[data-role="icon"]');

    icons.forEach((el) => {
        const icon = el.getAttribute('data-icon');
        if (icon.toLowerCase().indexOf(q) !== -1) {
            el.classList.remove('hidden');
        } else {
            el.classList.add('hidden');
        }
    });
}

function selectFontAwesomeIcon(icon, targetId) {
    const target = document.getElementById(targetId);
    const input = target.querySelector('input[data-role="input"]');
    input.value = icon;
    const selectedIcon = target.querySelector('*[data-role="selected-icon');
    const selectedIconLabel = target.querySelector('*[data-role="selected-icon-label');
    selectedIcon.setAttribute('class', 'icon-preview-icon ' + icon);
    selectedIconLabel.innerHTML = icon;
}
