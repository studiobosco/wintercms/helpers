<?php if ($this->hasFilledClipboard()): ?>
<button
    class="btn btn-secondary wn-icon-clipboard"
    type="button"
    <?php if ($this->canPaste()): ?>
    data-request="<?= $this->getEventHandler('onPaste'); ?>"
    data-request-confirm="<?= e(trans('studiobosco.helpers::clipboard.paste_confirm')); ?>"
    <?php else: ?>
    disabled
    <?php endif; ?>
>
    <?= e(trans($this->pasteLabel)); ?>
</button>

<button
    class="btn btn-secondary wn-icon-broom"
    type="button"
    data-request="<?= $this->getEventHandler('onClearClipboard'); ?>"
>
    <?= e(trans($this->clearLabel)); ?>
</button>
<?php else: ?>
<button
    class="btn btn-secondary wn-icon-clipboard"
    type="button"
    data-request="<?= $this->getEventHandler('onCopy'); ?>"
>
    <?= e(trans($this->copyLabel)); ?>
</button>
<?php endif; ?>
