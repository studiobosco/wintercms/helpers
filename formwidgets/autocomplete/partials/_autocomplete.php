<?php
$fieldOptions = $field->options();
$useSearch = $field->getConfig('showSearch', true);
$emptyOption = $field->getConfig('emptyOption', $field->placeholder);
$minLength = $field->getConfig('minLength', 2);
$ajaxHandler = $field->getConfig('suggestions');
?>

<!-- Dropdown -->
<?php if ($this->previewMode || $field->readOnly): ?>
    <div class="form-control" <?= $field->readOnly ? 'disabled="disabled"' : ''; ?>>
        <?= (isset($fieldOptions[$field->value])) ? e(trans($fieldOptions[$field->value])) : '' ?>
    </div>
    <input type="hidden" name="<?= $field->getFieldName() ?>" value="<?= $field->value ?>">
<?php else: ?>
    <input type="hidden" name="<?= $field->getFieldName() ?>" value="<?= $field->value ?>">
    <select
        id="<?= $field->getId() ?>"
        name="<?= $field->getFieldName() ?>"
        class="form-control custom-select <?= $useSearch ? '' : 'select-no-search' ?>"
        data-handler="<?= $this->getEventHandler('onGetSuggestions'); ?>"
        data-minimum-input-length="<?= $minLength; ?>"
        data-ajax-delay="300"
        <?= $field->placeholder ? 'data-placeholder="'.e(trans($field->placeholder)).'"' : '' ?>
        >
        <?php if ($emptyOption): ?>
            <option value=""><?= e(trans($emptyOption)) ?></option>
        <?php endif ?>
        <?php foreach ($fieldOptions as $value => $option): ?>
            <?php
            if (!is_array($option)) {
                $option = [$option];
            }
            ?>
            <option
                <?= $field->isSelected($value) ? 'selected="selected"' : '' ?>
                <?php if (isset($option[1])): ?>
                    data-<?= strpos($option[1], '.') ? 'image' : 'icon' ?>="<?= $option[1] ?>"
                <?php endif ?>
                value="<?= e($value) ?>"
            ><?= e(trans(is_array($option) ? $option[0] : $option)) ?></option>
        <?php endforeach ?>
    </select>
<?php endif ?>
