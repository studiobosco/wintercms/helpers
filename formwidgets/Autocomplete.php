<?php namespace StudioBosco\Helpers\FormWidgets;

use Backend\Classes\FormWidgetBase;

class Autocomplete extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'autocomplete';

    /**
     * Prepares the list data
     */
    public function prepareVars()
    {
        $this->vars['field'] = $this;
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('autocomplete');
    }

    public function onGetSuggestions()
    {
        $q = post('q');
        $suggestionsMethod = 'get' . studly_case($this->formField->fieldName) . 'Suggestions';

        $suggestions = $this->getConfig('suggestions', $suggestionsMethod);

        if (!is_array($suggestions) &&
            is_string($suggestions) &&
            $this->model->methodExists($suggestions)
        ) {
            $suggestions = $this->model->{$suggestions}($q);
        }

        return [
            'results' => $suggestions,
        ];
    }

    /**
     * Sets field options, for dropdowns, radio lists and checkbox lists.
     * @param  array $value
     * @return self
     */
    public function options()
    {
        $optionsMethod = 'get' . studly_case($this->formField->fieldName) . 'Options';
        $options = $this->getConfig('options', $optionsMethod);

        if (!is_array($options) &&
            is_string($options) &&
            $this->model->methodExists($options)
        ) {
            $options = $this->model->{$options}();
        } else {
            $options = [];
        }

        return $options;
    }

    /**
     * Determine if the provided value matches this field's value.
     * @param string $value
     * @return bool
     */
    public function isSelected($value = true)
    {
        if ($this->formField->value === null) {
            return false;
        }

        $currentValue = $this->formField->value;

        return (string) $value === (string) $currentValue;
    }
}
