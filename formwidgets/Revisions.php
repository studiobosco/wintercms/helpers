<?php namespace StudioBosco\Helpers\FormWidgets;

use Input;
use Lang;
use Carbon\Carbon;
use Backend\Classes\FormWidgetBase;
use Backend\Facades\BackendAuth;

class Revisions extends FormWidgetBase
{
    use \Backend\Traits\FormModelWidget;

    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'revisions';

    /**
     * Name of the revision relation
     *
     * @var string
     */
    public $relation = 'revision_history';

    /**
     * Field labels
     *
     * @var array
     */
    public $fields = [];

    /**
     * Date format
     *
     * @var string
     */
    public $dateFormat = 'Y/m/d - H:i:s';

    /**
     * User model class
     *
     * @var string
     */
    public $userModel = 'Backend\Models\User';

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->fillFromConfig([
            'relation',
            'fields',
            'dateFormat',
        ]);

        if (!is_array($this->fields)) {
            $this->fields = [];
        }

        $this->valueFrom = $this->relation;
    }

    /**
     * @inheritDoc
     */
    protected function loadAssets()
    {
        $this->addCss('css/revisions.css', 'studiobosco.helpers');
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('revisions');
    }

    /**
     * Prepares the list data
     */
    public function prepareVars()
    {
        $this->vars['history'] = $this->getHistory();
        $this->fields = $this->getFieldsConfig();
    }

    protected function getFieldsConfig()
    {
        $fieldsConfig = [];
        $allFields = $this->parentForm->getFields();

        foreach($allFields as $fieldName => $fieldConfig) {
            $fieldsConfig[$fieldName] = [
                'label' => $fieldConfig->label,
                'valueFrom' => $fieldConfig->valueFrom,
                'nameFrom' => $fieldConfig->config['nameFrom'] ?? null,
                'type' => $fieldConfig->config['type'] ?? $fieldConfig->type,
                'options' => $fieldConfig->config['options'] ?? null,
            ];
        }

        foreach($this->fields as $fieldName => $fieldConfig) {
            foreach($fieldConfig as $key => $value) {
                $fieldsConfig[$fieldName][$key] = $value;
            }
        }

        return $fieldsConfig;
    }

    protected function getHistory()
    {
        $relationObject = $this->getRelationObject();
        return $relationObject->get();
    }

    protected function getUserName($userId)
    {
        $userModelClass = $this->userModel;
        $user = $userModelClass ? $userModelClass::find($userId) : null;

        return $user ? $user->first_name . ' ' . $user->last_name : '';
    }

    protected function getUserEmail($userId)
    {
        $userModelClass = $this->userModel;
        $user = $userModelClass ? $userModelClass::find($userId) : null;

        return $user ? $user->email : '';
    }

    protected function getFieldLabel($fieldName)
    {
        // auto detect relations if the field is a foreign id field
        if (ends_with($fieldName, '_id')) {
            $relationName = substr($fieldName, 0, -3);
            $relationNameCamelCase = camel_case($relationName);

            if (isset($this->fields[$relationName])) {
                $fieldName = $relationName;
            } elseif (isset($this->fields[$relationNameCamelCase])) {
                $fieldName = $relationNameCamelCase;
            }
        } elseif(ends_with($fieldName, '_ids')) {
            $relationName = substr($fieldName, 0, -4);
            $relationNameCamelCase = camel_case($relationName);

            if (isset($this->fields[$relationName])) {
                $fieldName = $relationName;
            } elseif (isset($this->fields[$relationNameCamelCase])) {
                $fieldName = $relationNameCamelCase;
            }
        }

        return isset($this->fields[$fieldName])
            ? Lang::get($this->fields[$fieldName]['label'] ?? '')
            : $fieldName
        ;
    }

    protected function formatFieldValue($value, $fieldName)
    {
        // auto detect relations if the field is a foreign id field
        if (ends_with($fieldName, '_id')) {
            $relationName = substr($fieldName, 0, -3);
            $relationNameCamelCase = camel_case($relationName);

            if (isset($this->fields[$relationName])) {
                $fieldName = $relationName;
            } elseif (isset($this->fields[$relationNameCamelCase])) {
                $fieldName = $relationNameCamelCase;
            }
        } elseif(ends_with($fieldName, '_ids')) {
            $relationName = substr($fieldName, 0, -4);
            $relationNameCamelCase = camel_case($relationName);
            $value = is_string($value) ? json_decode($value, true) : $value;

            if (isset($this->fields[$relationName])) {
                $fieldName = $relationName;
            } elseif (isset($this->fields[$relationNameCamelCase])) {
                $fieldName = $relationNameCamelCase;
            }
        }

        $fieldConfig = $this->fields[$fieldName] ?? [];
        $type = $fieldConfig['type'] ?? 'text';

        switch($type) {
            case 'date':
            case 'datepicker':
                return e(Carbon::parse($value)->format(
                    Lang::get(
                        array_get($fieldConfig, 'dateFormat', $this->dateFormat)
                    )
                ));
                break;
            case 'partial':
                $path = $fieldConfig['path'] ?? null;
                if (!$path) return 'missing "path" in field config for "' . $fieldName . '".';
                return $this->makePartial($path, ['value' => $value, 'formModel' => $this->model]);
                break;
            case 'partialwithvars':
                $path = $fieldConfig['path'] ?? null;
                if (!$path) return 'missing "path" in field config for "' . $fieldName . '".';
                $vars = $fieldConfig['vars'] ?? [];
                return $this->makePartial($path, array_merge(['value' => $value, 'formModel' => $this->model], $vars));
                break;
            case 'recordfinder':
            case 'relation':
                $nameFrom = $fieldConfig['nameFrom'];
                $valueFrom = $fieldConfig['valueFrom'] ?? $fieldName;
                $relationModel = $this->model->$valueFrom()->getRelated();

                if (is_array($value)) {
                    $keyName = $this->getRelationKeyName($this->model->$valueFrom());
                    $records = $relationModel->whereIn($keyName, $value)->get();

                    return implode('<br/>', array_map(function ($id) use ($records, $nameFrom) {
                        $item = $records->find($id);
                        if (!$item) {
                            return '- ' . trans('studiobosco.helpers::lang.revisions.missing_related_record') . ' -';
                        } else {
                            return e($item->$nameFrom);
                        }
                    }, $value));
                } else {
                    $record = $relationModel->find($value);
                    return e($record ? $record->$nameFrom : $value);
                }
                break;
            case 'dropdown':
                $optionsMethod = 'get' . studly_case($fieldName) . 'Options';
                $options = [];

                if (!isset($fieldConfig['options']) || !$fieldConfig['options']) {
                    $options = $this->model->$optionsMethod();
                } elseif (is_string($fieldConfig['options'])) {
                    $optionsMethod = $fieldConfig['options'];
                    $options = $this->model->$optionsMethod();
                } elseif (is_array($fieldConfig['options'])) {
                    $options = $fieldConfig['options'];
                }

                $value = isset($options[$value]) ? $options[$value] : null;
                return is_array($value) ? '<i class="select-icon ' . e($value[1]) . '"></i> ' . e($value[0]) : e($value);
            case 'checkbox':
            case 'switch': {
                return $value
                    ? e(trans(array_get($fieldConfig, 'on', 'backend::lang.list.column_switch_true')))
                    : e(trans(array_get($fieldConfig, 'off', 'backend::lang.list.column_switch_false')))
                ;
            }
            default:
                return is_object($value) || is_array($value) ? e(json_encode($value, JSON_PRETTY_PRINT)) : e($value);
        }
    }

    protected function getRelationKeyName($relation)
    {
        if ($relation instanceof \Winter\Storm\Database\Relations\BelongsToMany) {
            return $relation->getRelatedKeyName();
        }
        if ($relation instanceof \Winter\Storm\Database\Relations\BelongsTo) {
            return $relation->getOwnerKeyName();
        }

        return $relation->getLocalKeyName();
    }

    public function onRevertChange()
    {
        $revisionId = Input::get('revision_id');
        $revisions = $this->getRelationObject();
        $revision = $revisions->find($revisionId);

        if ($revision) {
            $fieldName = $revision->field;
            $oldValue = $revision->old_value;
            if (in_array($fieldName, $this->model->getJsonable())) {
                $oldValue = json_decode($oldValue);
            }

            if(ends_with($fieldName, '_ids')) {
                $relName = substr($fieldName, 0, -4);

                if (in_array($relName, $this->model->revisionableRelations)) {
                    $relation = $this->model->{$relName}();

                    // has many relationships cannot be reverted, as the models are deleted when removed.
                    if ($relation instanceof \Winter\Storm\Database\Relations\HasMany) {
                        return;
                    }

                    $ids = $relation->select('id')->get()->pluck('id')->toArray();
                    $oldIds = is_string($oldValue) ? json_decode($oldValue ?? '[]', true) : $oldValue;

                    // remove related models that are not present in old IDs
                    foreach($ids as $id) {
                        if (!in_array($id, $oldIds)) {
                            $relation->detach($id);
                        }
                    }

                    // attach related models that are not present in current IDs
                    foreach($oldIds as $oldId) {
                        if (!in_array($oldId, $ids)) {
                            $relation->attach($oldId);
                        }
                    }

                    $this->model->save();
                }
            } else {
                $this->model->$fieldName = $oldValue;
                $this->model->save();
            }

            return redirect(request()->fullUrl());
        }
    }
}
