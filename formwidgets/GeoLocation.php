<?php namespace StudioBosco\Helpers\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Winter\Storm\Html\Helper as HtmlHelper;
use StudioBosco\Helpers\Classes\TwigHelper;

/**
 * GeoLocation Form Widget
 */
class GeoLocation extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'geolocation';

    public $latField = 'lat';
    public $lngField = 'lng';
    public $mapHeight = null;

    /**
     * Determines if repeater has been initialised previously
     *
     * @var boolean
     */
    protected $loaded = false;

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->fillFromConfig([
            'latField',
            'lngField',
            'mapHeight',
        ]);

        if ($this->formField->disabled) {
            $this->previewMode = true;
        }

        // Check for loaded flag in POST
        if ((bool) post($this->alias . '_loaded') === true) {
            $this->loaded = true;
        }
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('geolocation');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['model'] = $this->model;
        $this->vars['latFieldName'] = $this->getModelFieldName($this->latField ?? 'lat');
        $this->vars['lngFieldName'] = $this->getModelFieldName($this->lngField ?? 'lng');
    }

    protected function getModelFieldName($fieldName)
    {
        $arrayName = $this->formField->arrayName;
        return $arrayName.'['.implode('][', HtmlHelper::nameToArray($fieldName)).']';
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        $this->addCss('vendor/leaflet/leaflet.css', 'StudioBosco.Helpers');
        $this->addCss('vendor/leaflet/L.Control.Locate.min.css', 'StudioBosco.Helpers');
        $this->addJs('vendor/leaflet/leaflet.js', 'StudioBosco.Helpers');
        $this->addJs('vendor/leaflet/L.Control.Locate.min.js', 'StudioBosco.Helpers');

        if (!$this->preview) {
            $this->addCss('vendor/geocoder/Control.Geocoder.css', 'StudioBosco.Helpers');
            $this->addJs('vendor/geocoder/Control.Geocoder.js', 'StudioBosco.Helpers');
        }

        $this->addCss('css/geolocation.css', 'StudioBosco.Helpers');
        $this->addJs('js/geolocation.js', 'StudioBosco.Helpers');
    }

    public function getLoadValue()
    {
        $latFieldName = $this->getModelFieldName($this->latField ?? 'lat');
        $lngFieldName = $this->getModelFieldName($this->lngField ?? 'lng');
        $latFieldName = array_last(explode('[', $latFieldName, 2));
        $lngFieldName = array_last(explode('[', $lngFieldName, 2));
        $latField = str_replace(']', '', str_replace('[', '.', $latFieldName));
        $lngField = str_replace(']', '', str_replace('[', '.', $lngFieldName));
        $latValue = TwigHelper::getDeep($this->model, $latField, null);
        $lngValue = TwigHelper::getDeep($this->model, $lngField, null);
        $value = [
            'lat' => $latValue,
            'lng' => $lngValue,
        ];

        return $value;
    }

    /**
     * @inheritDoc
     */
    public function getSaveValue($value)
    {
        return $value;
    }
}
