<?php namespace StudioBosco\Helpers\FormWidgets;

use Session;
use Flash;
use Request;
use System\Classes\PluginManager;
use Backend\Classes\FormWidgetBase;

/**
 * Clipboard Form Widget
 */
class Clipboard extends FormWidgetBase
{
    protected $defaultLocale;
    public $targetField = null;
    public $targetModelClass = null;
    public string $copyLabel = 'studiobosco.helpers::clipboard.copy';
    public string $pasteLabel = 'studiobosco.helpers::clipboard.paste';
    public string $clearLabel = 'studiobosco.helpers::clipboard.clear';

    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'clipboard';

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->fillFromConfig([
            'copyLabel',
            'pasteLabel',
            'targetField',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('default');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['formField'] = $this->formField;
        $this->vars['formModel'] = $this->model;
        $this->vars['targetField'] = $this->targetField;
        $this->vars['targetModelClass'] = $this->targetModelClass = $this->model::class;

        if ($this->isTranslatable()) {
            $this->defaultLocale = \Winter\Translate\Models\Locale::getDefault()->code;
        }
    }

    public function isTranslatable()
    {
        $formField = $this->parentForm->getField($this->targetField);

        return $formField
            ? PluginManager::instance()->hasPlugin('Winter.Translate') && \Winter\Translate\Models\Locale::isAvailable() && $formField->getConfig('translatable')
            : false
        ;
    }

    public function hasFilledClipboard()
    {
        return Session::has('studiobosco.helpers.clipboard');
    }

    public function canPaste()
    {
        $clipboard = Session::get('studiobosco.helpers.clipboard');

        if (!$clipboard) {
            return false;
        }

        $this->prepareVars();

        return (
            array_get($clipboard, 'targetField') === $this->targetField &&
            array_get($clipboard, 'targetModelClass') === $this->targetModelClass
        );
    }

    public function onCopy()
    {
        $this->prepareVars();

        $targetField = str_replace(']', '', str_replace('[', '.', $this->targetField));
        $postData = post();

        if ($this->targetModelClass !== 'Winter\\Pages\\Classes\\Page') {
            $targetField = last(explode('\\', $this->targetModelClass)) . '.' . $targetField;
        }

        $data = array_get($postData, $targetField);

        $clipboard = [
            'targetField' => $this->targetField,
            'targetModelClass' => $this->targetModelClass,
            'data' => $data,
        ];

        Session::put('studiobosco.helpers.clipboard', $clipboard);

        return $this->refresh();
    }

    public function onPaste()
    {
        $this->prepareVars();

        $targetField = str_replace(']', '', str_replace('[', '.', $this->targetField));
        $targetFormField = $this->parentForm->getField($this->targetField);
        $value = array_get(Session::get('studiobosco.helpers.clipboard'), 'data');

        if ($this->canPaste() && $value) {
            if ($this->isTranslatable()) {
                $postData = post();
                $locale = (
                    array_get($postData, 'RLTranslateLocale.' . $targetField) ??
                    array_get($postData, 'RLTranslateRepeaterLocale.' . $targetField) ??
                    array_get($postData, 'RLTranslateNestedFormLocale.' . $targetField)
                );

                // if we have the default locale we can simply set the model attribute
                if ($locale === $this->defaultLocale) {
                    if ($this->objectMethodExists($this->model, 'setAttributeTranslated')) {
                        $this->model->setAttributeTranslated($this->targetField, $value, $locale);
                    }

                    $this->model->setAttribute($this->targetField, $value);
                    $this->rewritePostValues($value);

                    if ($targetFormField) {
                        $targetFormField->value = $value;
                    }
                }
                // when dealing with non default locale we need to use the translation method to set the attribute
                else {
                    if ($this->objectMethodExists($this->model, 'setAttributeTranslated')) {
                        $this->model->setAttributeTranslated($this->targetField, $value, $locale);
                    }

                    // UI will reset to default locale so we need to set the values back
                    $defaultLocaleValue = json_decode(array_get($postData, 'RLTranslate.' . $this->defaultLocale . '.' . $targetField), true);

                    if ($this->objectMethodExists($this->model, 'setAttributeTranslated')) {
                        $this->model->setAttributeTranslated($this->targetField, $defaultLocaleValue, $this->defaultLocale);
                    }

                    $this->model->setAttribute($this->targetField, $defaultLocaleValue);
                    $this->rewritePostValues($defaultLocaleValue);

                    if ($targetFormField) {
                        $targetFormField->value = $defaultLocaleValue;
                    }
                }
            } else {
                $this->model->setAttribute($this->targetField, $value);
            }

            return $this->refresh(true);
        }
    }

    public function onClearClipboard()
    {
        Session::forget('studiobosco.helpers.clipboard');
        $this->prepareVars();

        return $this->refresh();
    }

    protected function refresh($withTargetField = false)
    {
        if ($withTargetField) {
            $formField = $this->parentForm->getField($this->targetField);

            if ($formField) {
                $fieldType = $formField->getConfig('type');
                $renderedField = $this->parentForm->renderField($this->targetField);

                // nested forms must not include the wrapping element
                if (in_array($fieldType, ['nestedform', 'mlnestedform'])) {
                    $renderedField = explode('>', $renderedField, 2)[1];
                    $renderedFieldParts = explode('<', $renderedField);
                    array_pop($renderedFieldParts);
                    $renderedField = implode('<', $renderedFieldParts);
                }

                return [
                    '#' . $formField->getId('group') => $renderedField,
                    '#' . $this->getId('wrapper') => $this->makePartial('clipboard'),
                ];
            } else {
                return Flash::error(trans('studiobosco.helpers::clipboard.error_form_field_not_found', ['field' => $this->targetField]));
            }
        } else {
            return [
                '#' . $this->getId('wrapper') => $this->makePartial('clipboard'),
            ];
        }
    }

    protected function rewritePostValues($data, $locale = null)
    {
        $targetField = str_replace(']', '', str_replace('[', '.', $this->targetField));

        if ($this->targetModelClass !== 'Winter\\Pages\\Classes\\Page') {
            $targetField = last(explode('\\', $this->targetModelClass)) . '.' . $targetField;
        }

        if ($locale) {
            $fieldName = 'RLTranslate.' . $locale . '.' . $targetField;
            $data = json_encode($data);
        } else {
            $fieldName = $targetField;
        }

        $requestData = Request::all();
        array_set($requestData, $fieldName, $data);
        Request::merge($requestData);
    }

    /**
     * Internal helper for method existence checks.
     *
     * @param  object $object
     * @param  string $method
     * @return boolean
     */
    protected function objectMethodExists($object, $method)
    {
        if (method_exists($object, 'methodExists')) {
            return $object->methodExists($method);
        }

        return method_exists($object, $method);
    }
}
