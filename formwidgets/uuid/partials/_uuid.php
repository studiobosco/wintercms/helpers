<input
    <?php if ($this->hidden): ?>
    type="hidden"
    <?php else: ?>
    class="form-control"
    type="text"
    readonly
    <?php endif; ?>
    name="<?= $name; ?>"
    value="<?= $value; ?>"
/>
