<?php namespace StudioBosco\Helpers\FormWidgets;

use Input;
use Lang;
use Carbon\Carbon;
use Backend\Classes\FormWidgetBase;
use Backend\Facades\BackendAuth;

use StudioBosco\Helpers\Classes\FormHelper;

class FontAwesomeIcon extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'fontawesomeicon';

    /**
     * @inheritDoc
     */
    protected function loadAssets()
    {
        $this->addCss('css/fontawesomeicon.css', 'studiobosco.helpers');
        $this->addJs('js/fontawesomeicon.js', 'studiobosco.helpers');
    }

    protected function prepareVars()
    {
        $this->vars['options'] = FormHelper::getFontAwesomeIconOptions();
        $this->vars['value'] = $this->getLoadValue();
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('fontawesomeicon');
    }
}
