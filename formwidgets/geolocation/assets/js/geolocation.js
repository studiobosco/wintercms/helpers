window.GeoLocationWidget = function (el, opts) {
  const mapEl = el.querySelector('*[data-role="map"]');
  const map = L.map(mapEl, {
    scrollWheelZoom: false,
  });
  const marker = L.marker([opts.lat || 0, opts.lng || 0], {
    draggable: opts.preview ? false : true,
  }).addTo(map);

  L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    subdomains: ['a', 'b', 'c'],
  }).addTo(map);

  if (opts.lat && opts.lng) {
    map.setView([opts.lat, opts.lng], 10);
  } else if (!opts.preview) {
    map.setView([0, 0], 10);
    map.locate({
      setView: true,
    });
  }

  if (opts.preview) {
    return;
  }

  const localteControl = L.control.locate({
    showCompass: false,
    drawCircle: false,
    drawMarker: false,
  }).addTo(map);

  const geocoderControl = L.Control.geocoder({
    defaultMarkGeocode: false,
    suggestTimeout: 500,
    suggestMinLength: 3,
    collapsed: false,
    showResultIcons: true,
    placeholder: opts.placeholder || 'Search',
  })
  .on('markgeocode', (event) => {
    map.fitBounds(event.geocode.bbox);
    const center = event.geocode.center;
    placeMarker(center.lat, center.lng);
  })
  .addTo(map);

  geocoderControl._input.addEventListener('input', debounce(() => {
    if (geocoderControl._input.value.trim().length >= 3) {
      geocoderControl._geocode();
    } else {
      geocoderControl._clearResults();
    }
  }, 500));

  map.on('click', (event) => {
    placeMarker(event.latlng.lat, event.latlng.lng);
  });

  function placeMarker(lat, lng) {
    marker.setLatLng([lat, lng]);
  }

  marker.on('move', () => {
    const latLng = marker.getLatLng();
    const lat = latLng.lat;
    const lng = latLng.lng;

    if (!opts.latField || !opts.lngField) {
      return;
    }

    const latField = document.querySelector(opts.latField);
    const lngField = document.querySelector(opts.lngField);

    if (!latField || !lngField) {
      return;
    }

    latField.value = lat;
    lngField.value = lng;

    latField.dispatchEvent(new UIEvent('change'));
    lngField.dispatchEvent(new UIEvent('change'));
  });

  setTimeout(() => {
    if (!opts.latField || !opts.lngField) {
      return;
    }

    const latField = document.querySelector(opts.latField);
    const lngField = document.querySelector(opts.lngField);

    if (!latField || !lngField) {
      return;
    }

    function updateFromInput() {
      const lat = latField.value;
      const lng = lngField.value;

      placeMarker(lat, lng);
      map.setView([lat, lng]);
    }

    latField.addEventListener('input', updateFromInput);
    lngField.addEventListener('input', updateFromInput);
  }, 500);

  const visibilityObserver = new IntersectionObserver((entries) => {
    entries.forEach((entry) => {
      if (entry.target === el && entry.isIntersecting) {
        map.invalidateSize();
      }
    });
  });

  visibilityObserver.observe(el);

  function debounce(func, timeout = 300){
    let timer;
    return (...args) => {
      clearTimeout(timer);
      timer = setTimeout(() => { func.apply(this, args); }, timeout);
    };
  }
};
