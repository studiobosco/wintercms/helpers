<?php namespace StudioBosco\Helpers\Models;

use Model;

/**
 * Settings Model
 */
class Settings extends Model
{

    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'studiobosco_helpers';

    public $settingsFields = 'fields.yaml';

    public function afterSave()
    {
        if (self::get('use_media_images_in_blog_posts', false)) {
            $migration = new \StudioBosco\Helpers\Database\Migrations\AddFeaturedImagesToBlogpostsTable();
            $migration->up();
        }
    }
}
