<?php namespace StudioBosco\Helpers\Console;

use Winter\Storm\Console\Command;
use League\Csv\Writer as CsvWriter;
use League\Csv\EscapeFormula as CsvEscapeFormula;
use SplTempFileObject;

class ExportTranslations extends Command
{
    /**
     * @var string The console command name.
     */
    protected static $defaultName = 'helpers:export:translations';

    /**
     * @var string The name and signature of this command.
     */
    protected $signature = 'helpers:export:translations
        {translationNamespace : Translation namespace. <info>E.g. `winter.pages::lang`</info>}
        {locale : The locale to export.}
        {--format=csv : Format to output. Default "csv". Available Options "csv", "json"}
        {--namespace-is-path : if set the namespace is a considered to be a file path}';

    /**
     * @var string The console command description.
     */
    protected $description = 'Exports a translation file.';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $translationNamespace = $this->argument('translationNamespace');
        $local = $this->argument('locale');
        $format = $this->option('format', 'csv');
        $namespaceIsPath = $this->option('namespace-is-path');

        $translationFilePath = $namespaceIsPath
            ? $translationNamespace
            : $this->resolveTranslationFilePath($translationNamespace, $local)
        ;

        if (!$translationFilePath || !file_exists($translationFilePath)) {
            return $this->error('Invalid translation path or file not found.');
        }

        $contents = require($translationFilePath);

        switch($format) {
            case 'csv': {
                echo $this->convertToCsv($contents);
                break;
            }
            case 'json': {
                echo $this->convertToJson($contents);
                break;
            }
        }
    }

    protected function resolveTranslationFilePath($translationNamespace, $locale)
    {
        // convert to relative path
        $translationFilePath = str_replace('.', '/', $translationNamespace);
        $translationFilePath = str_replace('::', '/lang/' . $locale . '/', $translationFilePath) . '.php';

        // look in system modules first
        $finalPath = base_path('modules/' . $translationFilePath);

        if (file_exists($finalPath)) {
            return $finalPath;
        }

        // look in plugins
        $finalPath = plugins_path($translationFilePath);

        if (file_exists($finalPath)) {
            return $finalPath;
        }

        return null;
    }

    protected function convertToCsv(array $contents)
    {
        $flatContents = array_dot($contents);
        $items = [];

        foreach($flatContents as $key => $value) {
            $items[] = ['key' => $key, 'message' => $value];
        }

        $csv = CsvWriter::createFromFileObject(new SplTempFileObject);
        $csv->setOutputBOM(CsvWriter::BOM_UTF8);
        $csv->setDelimiter(',');
        $csv->setEnclosure('"');
        $csv->setEscape('\\');
        $csv->addFormatter(new CsvEscapeFormula());
        $csv->insertOne(['Key', 'Message']);
        $csv->insertAll($items);

        return (string) $csv;
    }

    protected function convertToJson(array $contents)
    {
        return json_encode($contents, JSON_PRETTY_PRINT);
    }
}
