<?php namespace StudioBosco\Helpers\Traits;

use InvalidArgumentException;
use StudioBosco\Helpers\Database\Relations\HasManyFromArray;

trait WithHasManyFromArrayRelation
{
    public static function bootWithHasManyFromArrayRelation()
    {
        static::extend(function ($model) {
            foreach($model->getHasManyFromArrayRelations() as $relation => $relationConfig) {
                $localKey = $relationConfig['key'];

                $model->addCasts([
                    $localKey => 'array',
                ]);
            }
        });
    }

    public function hasManyFromArray($related, $foreignKey = null, $localKey = null, $relationName = null)
    {
        $instance = new $related();
        return new HasManyFromArray($instance->newQuery(), $this, $foreignKey, $localKey, $relationName);
    }

    protected function getHasManyFromArrayRelations()
    {
        $relations = isset($this->hasManyFromArray) && is_array($this->hasManyFromArray)
            ? $this->hasManyFromArray
            : [];

        foreach($relations as $key => $config) {
            $config['key'] = $config['key'] ?? snake_case(str_singular($key)) . '_ids';
            $config['otherKey'] = $config['otherKey'] ?? 'id';
            $relations[$key] = $config;
        }

        return $relations;
    }

    public function getRelationType(string $name, bool $includeMethods = true): ?string
    {
        $relations = $this->getHasManyFromArrayRelations();

        if (in_array($name, array_keys($relations))) {
            return 'hasMany';
        } else return parent::getRelationType($name, $includeMethods);
    }

    public function getRelationDefinition(string $name, bool $includeMethods = true): ?array
    {
        $relations = $this->getHasManyFromArrayRelations();

        if (in_array($name, array_keys($relations))) {
            return $relations[$name] ?? null;
        } else return parent::getRelationDefinition($name, $includeMethods);
    }

    protected function addRelation(string $type, string $name, array $config): void
    {
        if ($type === 'hasManyFromArray') {
            if ($this->hasRelation($name) || isset($this->{$name})) {
                throw new InvalidArgumentException(
                    sprintf(
                        'Cannot add the "%s" relation to %s, it conflicts with an existing relation, attribute, or property.',
                        $name,
                        get_class($this)
                    )
                );
            } else {
                $this->{$type} = array_merge($this->{$type}, [$name => $config]);
            }
        } else {
            parent::addRelation($type, $name, $config);
        }
    }

    protected function handleRelation(string $relationName, bool $addConstraints = true): \Illuminate\Database\Eloquent\Relations\Relation
    {
        $relations = $this->getHasManyFromArrayRelations();

        if (in_array($relationName, array_keys($relations))) {
            $relationConfig = $relations[$relationName];
            $definition = $this->getRelationDefinition($relationName);
            $relation = $this->hasManyFromArray($relationConfig[0], $relationConfig['key'], $relationConfig['otherKey'], $relationName);

            // Add relation name
            $relation->setRelationName($relationName);

            // Add dependency, if required
            if (
                ($definition['delete'] ?? false) === true
                && in_array(
                    \Winter\Storm\Database\Relations\Concerns\CanBeDependent::class,
                    class_uses_recursive($relation)
                )
            ) {
                $relation = $relation->dependent();
            }

            // Add soft delete, if required
            if (
                ($definition['softDelete'] ?? false) === true
                && in_array(
                    \Winter\Storm\Database\Relations\Concerns\CanBeSoftDeleted::class,
                    class_uses_recursive($relation)
                )
            ) {
                $relation = $relation->softDeletable();
            }

            // Remove detachable, if required
            if (
                ($definition['detach'] ?? true) === false
                && in_array(
                    \Winter\Storm\Database\Relations\Concerns\CanBeDetachable::class,
                    class_uses_recursive($relation)
                )
            ) {
                $relation = $relation->notDetachable();
            }

            // Remove pushable flag, if required
            if (
                ($definition['push'] ?? true) === false
                && in_array(
                    \Winter\Storm\Database\Relations\Concerns\CanBePushed::class,
                    class_uses_recursive($relation)
                )
            ) {
                $relation = $relation->notPushable();
            }

            // Add count only flag, if required
            if (
                ($definition['count'] ?? false) === true
                && $addConstraints
                && in_array(
                    \Winter\Storm\Database\Relations\Concerns\CanBeCounted::class,
                    class_uses_recursive($relation)
                )
            ) {
                $relation = $relation->countOnly();
            }

            $relation->addDefinedConstraintsToRelation();

            if ($addConstraints) {
                // Add defined constraints
                $relation->addDefinedConstraintsToQuery();
            }

            return $relation;
        } else {
            return parent::handleRelation($relationName, $addConstraints);
        }
    }
}
