<?php namespace StudioBosco\Helpers\Traits;

trait NestedCasts
{
    public function attributesToArray()
    {
        $attributes = parent::attributesToArray();

        $nestedCasts = array_filter($this->casts, function ($value, $key) {
            return strpos($key, '.') > 0;
        }, ARRAY_FILTER_USE_BOTH);

        foreach($nestedCasts as $key => $value) {
            $attributeValue = array_get($attributes, $key);
            $castType = $this->getCastType($key);

            // set to null if value does not exist or value is an empty string while cast type is not string
            if (
                !array_has($attributes, $key) ||
                ($castType !== 'string' && is_string($attributeValue) && strlen($attributeValue) === 0)
            ) {
                array_set(
                    $attributes, $key, null
                );
            } else {
                $attributeValue = array_get($attributes, $key);
                array_set(
                    $attributes, $key,
                    $this->castAttribute($key, $attributeValue)
                );
            }
        }

        return $attributes;
    }
}
