<?php namespace StudioBosco\Helpers\Traits;

trait Activatable
{
    public function activate()
    {
        $this->is_active = true;
        $this->save();
    }

    public function deactivate()
    {
        $this->is_active = false;
        $this->save();
    }

    public function scopeIsActive($query)
    {
        return $query->where('is_active', true);
    }

    public function scopeIsInActive($query)
    {
        return $query->where('is_active', false);
    }
}
