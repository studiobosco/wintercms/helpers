<?php namespace StudioBosco\Helpers\Traits;

use Db;
use DateTime;
use BackendAuth;
trait RevisionableExtended
{
    public function getChangesFor($field)
    {
        $relation = $this->getRevisionHistoryName();
        $relationObject = $this->{$relation}();
        return $relationObject->where('field', $field)->orderBy('created_at', 'desc')->get();
    }

    public function getLastChangeFor($field)
    {
        $relation = $this->getRevisionHistoryName();
        $relationObject = $this->{$relation}();
        return $relationObject->where('field', $field)->orderBy('created_at', 'desc')->first();
    }

    public function getLastValueFor($field)
    {
        $lastChange = $this->getLastChangeFor($field);

        if ($lastChange) {
            return $lastChange->new_value;
        }

        return null;
    }

    public function getRevisionableUser()
    {
        return BackendAuth::getUser();
    }

    /**
     * Boot the revisionable trait for a model.
     * @return void
     */
    public static function bootRevisionableExtended()
    {
        static::extend(function ($model) {
            // add relation id attributes to revisionable, jsonable and purgeable
            if (!$model->revisionableRelations) {
                return;
            }

            foreach($model->revisionableRelations as $rel) {
                $attr = $rel . '_ids';
                $model->revisionable[] = $attr;
                $model->jsonable[] = $attr;
                // $model->purgeable[] = $attr;
            }

            $model->bindEvent('model.saveInternal', function () use ($model) {
                $model->revisionableExtendedStoreRelationIds();
            });
        });
    }

    public function revisionableExtendedStoreRelationIds()
    {
        if (!$this->revisionsEnabled) {
            return;
        }

        foreach($this->revisionableRelations as $rel) {
            $this->storeRelationIdsInAttribute($rel);
        }
    }

    public function storeRelationIdsInAttribute($rel)
    {
        $relation = $this->{$rel}();
        $revisionRelation = $this->getRevisionHistoryName();
        $revisionRelationObject = $this->{$revisionRelation}();
        $attr = $rel . '_ids';
        $lastIds = json_decode($this->getLastValueFor($attr) ?? '[]', true);
        $keyName = $this->getRelationKeyName($relation);
        $ids = ($keyName != 'id' ? $relation->select($keyName) : $relation)->get()->pluck($keyName)->toArray();

        $revisionModel = $revisionRelationObject->getRelated();

        if ($lastIds != $ids && $this->getKey()) {
            $toSave[] = [
                'field' => $attr,
                'old_value' => json_encode($lastIds),
                'new_value' => json_encode($ids),
                'revisionable_type' => $revisionRelationObject->getMorphClass(),
                'revisionable_id' => $this->getKey(),
                'user_id' => $this->revisionableGetUser(),
                'cast' => $this->revisionableGetCastType($attr),
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ];

            Db::table($revisionModel->getTable())->insert($toSave);
            $this->revisionableCleanUp();
        }
    }

    protected function getRelationKeyName($relation)
    {
        if ($relation instanceof \Winter\Storm\Database\Relations\BelongsToMany) {
            return $relation->getRelatedKeyName();
        }
        if ($relation instanceof \Winter\Storm\Database\Relations\BelongsTo) {
            return $relation->getOwnerKeyName();
        }

        return $relation->getLocalKeyName();
    }
}
