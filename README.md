# Helpers Plugin

WinterCMS Plugin that provides various helpers.

## Twig

## Filters

- `inline_file` renders the raw contents of a file\
  Usage: `'path/to/file' | inline_file`
- `file_hash` returns the hash of a files contents\
  Usage: `'path/to/file' | file_hash`
- `style_for` outputs the value for a (nested) key from `yourtheme/assets/styles.json`\
  Usage: `'some.key' | style_for`
- `url_without_host` removes the host and protocol form any url\
  Usage: `'http://foo.bar/path?query=1' | url_without_host` will result in `'/path?query=1'`
- `localized_file_path` will add a filename suffix containing the current or given locale to a file name\
  Usage: `'path/to/file.txt' | localized_file_path('en')` will result in `'path/to/file_en.txt'`
- `localized_url` will localize/prefix an URL with the current or given locale\
  Usage: `'/some/url' | localized_url` will result in `'/en/some/url'`
- `locale_date` will localize a date using the intl date formatter with a given format and optional locale or current locale\
  Usage: `'2022-01-01' | locale_date('MM/dd/yyyy', 'en')`
- `base64_encode` will output base64 encoded string\
  Usage: `'some string' | base64_encode`\
- `base64_encode_file` will output base64 encoded contents of a file\
  Usage: `'path/to/file.png' | base64_encode_file`\
  If the path is an url, you must pass `true` as first argument: `'/url/to/file.png' | base64_encode_file(true)`
- `md5` outputs a md5 hash of the given input\
  Usage: `'some-string' | md5`
- `sha1` outputs a sha1 hash of the given input\
  Usage: `'some-string' | sha1`
- `url_decode` decodes an encoded URL
- `youtube_video_id` determines the video id from a YouTube video URL\
  Usage: `'https://www.youtube.com/watch?v=xxx' | youtube_video_id` will result in `'xxx'`
- `vimeo_video_id` determines the video id from a Vimeo video URL\
  Usage: `'https://vimeo.com/123456' | vimeo_video_id` will result in `'123456'`
- `currency` formats a currency with the given currency code and locale\
  Usage: `100 | currency('EUR', 'de')` will result in `100,00 €`
### Functions

- `inline_file` renders the raw contents of a file\
  Usage: `inline_file('path/to/file')`
- `file_hash` returns the hash of a files contents\
  Usage: `file_hash('path/to/file')`
- `style_for` outputs the value for a (nested) key from `yourtheme/assets/styles.json`\
  Usage: `style_for('some.key')`
- `child_pages_with_layout` returns a list of static child pages with a certain layout\
  Usage: `child_pages_with_layout(page, 'default')`
  By default it will only return visible pages. To get a list of all pages do: `child_pages_with_layout(page, 'default', false)`
- `page_by_name` return a static page with the given name\
  Usage: `page_by_name('somepage')`
- `inject` will return a new instance of a given class with optional arguments\
  Usage: `inject('StudioBosco\Helpers\Classes\FormHelper', ...)`
- `render_component` will render a component with given properties. requires a cms or static page object to work.\
  Usage: `render_component('blogPosts', page, {postsPerPage: 10})`
- `array_set` will set the value of a key in an array\
  Usage: `array_set(some_array, 'some.key', 'some value')`
- `array_get` will get the value of a key in an array\
  Usage: `array_get(some_array, 'some.key', 'some default value')`
- `set_deep` will set a nested value of an array or object\
  Usage: `set_deep(some_array_or_object, 'some.key', 'some value')`
- `get_deep` will get a nested value of an array or object\
  Usage: `get_deep(some_array_or_object, 'some.key', 'some default value')`
- `current_url` will return the current url
- `url_without_host` removes the host and protocol form any url\
  Usage: `url_without_host('http://foo.bar/path?query=1')` will result in `'/path?query=1'`
- `locale` will return the current locale\
- `localized_url` will localize/prefix an URL with the current or given locale\
  Usage: `localized_url('/some/url')` will result in `'/en/some/url'`
- `query_param` will return the value of a given query parameter\
  Usage: `query_param('search')`
- `youtube_video_id`determines the video id from a YouTube video URL\
  Usage: `youtube_video_id'https://www.youtube.com/watch?v=xxx')` will result in `'xxx'`
- `vimeo_video_id` determines the video id from a Vimeo video URL\
  Usage: `vimeo_video_id'https://vimeo.com/123456')` will result in `'123456'`
- `carbon_date` converts anything into a carbon date\
  Usage: `carbon_date('now')`
- `config` retrieves a config value with an optional default value if not set\
  Usage: `config('some.key', 'default value')`
- `is_logged_in_backend` returns true if current user is logged into backend
- `image_size` returns an array with size of an image file\
  Usage: `image_size('/absolute/path/to/100x100_image.jpg')` will result in `['width' => 100, 'height' => 100, 'aspect_ratio' => 1]`
- `currency` formats a currency with the given currency code and locale\
  Usage: `currency(100, 'EUR', 'de')` will result in `100,00 €`

## Helpers

### ArrayHelper

- `StudioBosco\Helpers\Classes\ArrayHelper::uniqueBy($array, $path)` returns new array with unique values found under the given path.

### CountryHelper

- `StudioBosco\Helpers\Classes\CountryHelper::getCountries($locale = null)` return translated list of countries for the current or the given locale.

### CurrencyHelper

- `StudioBosco\Helpers\Classes\CurrencyHelper::getCurrencies()` return list of currencies.
- `StudioBosco\Helpers\Classes\CurrencyHelper::format(float $amount, string $currencyCode, $locale = null)` formats a given amount for a currency for the given locale.
### FormHelper

- `StudioBosco\Helpers\Classes\FormHelper::getFontAwesomeIconOptions()` returns a list of font awesome icons for use in a dropdown field.

### LocaleHelper

- `StudioBosco\Helpers\Classes\LocaleHelper::getLocale()` get's the current frontend locale
- `StudioBosco\Helpers\Classes\LocaleHelper::hasTranslatePlugin()` returns true if winter translate plugin is enabled
- `StudioBosco\Helpers\Classes\LocaleHelper::hasMulitpleLocales()` returns true if mulitple frontend locales are enabled

### SocialMediaHelper

- `StudioBosco\Helpers\Classes\SocialMediaHelper::getYouTubeVideoId($url)` retrieves the video ID for a YouTube video URL
- `StudioBosco\Helpers\Classes\SocialMediaHelper::getVimeoVideoId($url)` retrieves the video ID for a Vimeo video URL

### StaticPagesHelper

- `StudioBosco\Helpers\Classes\StaticPagesHelper::getStaticPageOptions()` returns a list of static pages for use in a dropdown field.
- `StudioBosco\Helpers\Classes\StaticPagesHelper::getChildPagesWithLayout($page, $layout, $visibleOnly = true)` returns a list of static child pages with the given layout.
- `StudioBosco\Helpers\Classes\StaticPagesHelper::getPageByName($name)` retrieves a page with the given name.

### BlogHelper

- `StudioBosco\Helpers\Classes\BlogHelper::getAllBlogCategoryOptions()` returns a list of blog categories in a dropdown field.
- `StudioBosco\Helpers\Classes\BlogHelper::getAllBlogPageOptions()` returns a list of blog posts in a dropdown field.
- `StudioBosco\Helpers\Classes\BlogHelper::getAllBlogTagOptions()` returns a list of tags (blog taxonomy) in a tags field.

### UrlHelper

- `StudioBosco\Helpers\Classes\UrlHelper::currentUrl()` retrieves the current request URL
- `StudioBosco\Helpers\Classes\UrlHelper::urlWithoutHost($url)` removes the host and protocol form any url
- `StudioBosco\Helpers\Classes\UrlHelper::localizedUrl($url, $withoutHost = true)` will localize/prefix an URL with the current or given locale


## Form widgets

### fontawesomeicon

Displays a searchable list of font awesome icons.

Usage:

```yaml
some_icon_field:
    type: fontawesomeicon
```

### geolocation

Displays a map where one can search for an address and/or place a marker to get latitude, longitude values. It will automaticly set the values of the given lat and lng field.

Usage:

```yaml
my_geolocation_field:
    type: geolocation
    latField: lat # 'lat' is default
    lngField: lng # 'lng' is default
    mapHeight: 400px
```

### revisions

Displays a list of revisions for a model form. Allows to undo a revision by inverting the changes and committing it on top of the revision history.

Usage:

First the model needs to be revisionable:

```php
class MyModel extends Model
{
    use \Winter\Storm\Database\Traits\Revisionable;
    use \Winter\Storm\Database\Traits\Purgeable; // also add this

    protected $revisionable = [
        'title',
        'description',
    ];

    protected $purgeable = [
        'revisions', // add revisions field to purgable or winter will try to store revisions on model save.
    ];

    public $morphMany = [
        'revision_history' => [
            'System\Models\Revision',
            'name' => 'revisionable',
            'order' => 'created_at desc',
        ],
    ];
}
```

Then you need to add it as a field.
It will display the field values based on theire type (if present in the model form).
You can however override fields. This is especially usefull for relational fields that would

```yaml
_revisions:
    type: revisions
    relation: revision_history # 'revision_history' is default
    dateFormat: Y/m/d - H:i:s # 'Y/m/d - H:i:s' is default
    fields: # override some fields
        some_related_model:
            nameFrom: title
            valueFrom: my_id
        some_special_field:
            label: 'Some special field'
            type: partial
            path: $/path/to/partial.htm
```

### partialwithvars

The same as the partials form widget, but you can add `vars` to it.
In the partials these vars will be available as php variables.

Usage:

```yaml
my_partial_with_vars:
    type: partialwithvars
    path: _some_partial.htm
    vars:
        key: value
        otherKey: otherValue
        ...
```

### autocomplete

Displays a list an autocomplete dropdown.

Usage:

Static suggestions:

```yaml
my_autocomplete:
    type: autocomplete
    suggestions:
        first: First
        second: Second
        last: Last
```

Dynamic suggestions:

```yaml
my_autocomplete:
    type: autocomplete
    suggestions: getMyAutocompleteSuggestions # you can use a custom method
```

```php
class MyModel extends Model
{
    /**
     * @param string $q - the search query
     * @return {array} the autocomplete suggestions
     */
    public function getMyAutocompleteSuggestions(string $q)
    {
        // return list with keys beeing the ID and value the label of the suggestion
    }
}
```

### Clipboard

The clipboard widget allows to add a copy and paste button that will copy contents from a `targetField` from one record or even locale into another one.
It will only allow pasting into the same model type and field with the model.

```yaml
some_nested_form:
  label: Just some nested form
  type: nestedform

clipboard_some_nested_form:
  copyLabel: Copy nested form
  pasteLabel: Paste nested form
  type: clipboard
  targetField: some_nested_form // use array-like syntax for nested fields. e.g. parentField[nestedField][evenDeeperNestedField]
```

### Uuid

The uuid widget allows to generate an UUID.
By default it is a hidden field. It is always readonly.

```yaml
my_uuid:
  label: UUID
  type: uuid
  hidden: false # set to false to show the field
```

## Model Traits

### RevisionableExtended

This trait adds some usefull features for revisionable models and also support for revisioning n-to-n relations.

Usage:

```php
class MyModel extends Model
{
    use \StudioBosco\Helpers\Traits\RevisionableExtended; // must come befre regular revionsable trait
    use \Winter\Storm\Database\Traits\Revisionable;

    protected $revisionableRelations = [
        'comments', // comments are now revisionable also they are 1-to-n
    ];

    public $hasMany = [
        'comments' => [ ... ],
    ];
}
```

### NestedCasts

This traits will cast nested attributes that are stored in e.g. JSON fields.

Usage:

```php
class MyModel extends Models
{
    use \StudioBosco\Helpers\Traits\NestedCasts;

    protected $casts = [
      'some.nested.attribute' => 'boolean', // This attribute will be cast to the type or become null if it does not exist.
    ];
}
```

## Other

### Default values for daterange filter scopes

You can now use default values for daterange list filters:

```
scopes:
    my_daterange:
        type: daterange
        default:
            - 'now'
            - 'now + 1 year'
```
