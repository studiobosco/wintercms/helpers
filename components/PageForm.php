<?php namespace StudioBosco\Helpers\Components;

use Cms\Classes\ComponentBase;

class PageForm extends ComponentBase
{
    /**
     * Gets the details for the component
     */
    public function componentDetails()
    {
        return [
            'name'        => 'studiobosco.helpers::pageform.name',
            'description' => 'studiobosco.helpers::pageform.description',
        ];
    }

    /**
     * Returns the properties provided by the component
     */
    public function defineProperties()
    {
        return [
            'form' => [
                'title' => 'studiobosco.helpers::pageform.form.title',
                'description' => 'studiobosco.helpers::pageform.form.description',
            ],
        ];
    }
}
