<?php return [
    'copy' => 'Copy',
    'paste' => 'Paste',
    'clear' => 'Clear',
    'paste_confirm' => 'This will overwrite all existing data.',
    'error_form_field_not_found' => 'Formfield :field was not found.',
];
