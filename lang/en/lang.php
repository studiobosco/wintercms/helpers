<?php return [
  'plugin' => [
    'name' => 'Helpers',
    'description' => 'Provides variaous helpers and twig extensions.',
  ],
  'formwidgets' => [
    'revisions' => 'Versions',
  ],
  'revisions' => [
    'missing_related_record' => 'Entry no longer exists.',
  ],
  'old_value' => 'From',
  'new_value' => 'To',
  'changed' => 'changed',
  'changed_by' => 'changed by :name',
  'revert_change' => 'Revert this change',
  'search' => 'Suchen',
  'settings' => [
    'label' => 'Helpers',
    'description' => 'Settings for helpers plugin.',
    'use_media_images_in_blog_posts' => 'Use images from media library for blog posts.',
  ],
];
