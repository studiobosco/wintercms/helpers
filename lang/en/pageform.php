<?php return [
    'name' => 'Page form',
    'description' => 'Adds fields to the page edit form. Must be placed in a page layout!',
    'form' => [
        'title' => 'Form YAML file',
        'description' => 'Path to the YAML file with form config, relativ to the page layout file.',
    ],
];
