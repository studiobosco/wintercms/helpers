<?php return [
  'plugin' => [
    'name' => 'Hilfsmittel',
    'description' => 'Stellt diverse Hilfsmittel und Twig Erweiterungen bereit.',
  ],
  'formwidgets' => [
    'revisions' => 'Versionen',
  ],
  'revisions' => [
    'missing_related_record' => 'Eintrag existiert nicht mehr.',
  ],
  'old_value' => 'Von',
  'new_value' => 'Zu',
  'changed' => 'geändert',
  'changed_by' => 'geändert von :name',
  'revert_change' => 'Änderung rückgängig machen',
  'search' => 'Suchen',
  'settings' => [
    'label' => 'Hilfsmittel',
    'description' => 'Einstellungen für Hilfsmittel',
    'use_media_images_in_blog_posts' => 'Bilder aus Medienbibliothek in Blog-Artikeln verwenden.',
  ],
];
