<?php return [
    'copy' => 'Kopieren',
    'paste' => 'Einfügen',
    'clear' => 'Leeren',
    'paste_confirm' => 'Dies wird alle bestehenden Daten überschreiben.',
    'error_form_field_not_found' => 'Formularfeld :field wurde nicht gefudnen.',
];
