<?php return [
    'name' => 'Seitenformular',
    'description' => 'Fügt dem Seitenformular Felder hinzu. Muss in einem Seitenlayout platziert werden!',
    'form' => [
        'title' => 'Formular YAML Datei',
        'description' => 'Pfad zur YAML Datei mit Formularkonfiguration, relativ zum Seitenlayout.',
    ],
];
